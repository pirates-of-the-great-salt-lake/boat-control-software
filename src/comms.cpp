#include "comms.h"
#include <thread>
#include <chrono>
#include <fstream>
#include <ctime>
#include <string.h>
#include <cstring>
#include <iostream>
#include <sstream>
#include <mutex>
#include <queue>
#include <algorithm>
#include <unordered_set>
#include <math.h>

#define MAX_BUFFER 1024

// buffer to hold and transfer tcpip traffic
static char tcpip_buffer[MAX_BUFFER];

// determines if sending using tcpip or not. Int since set with 'c' code.
static int tcpip_enabled = 0;

// prevent concurrent logging 
static std::mutex log_mtx;

// time in seconds to automatically send a report
static uint16_t report_frequency = 60;

// keeps track of outgoing message_ids for APRS
static int xmt_message_id = 0;

// keep track of all incoming message_ids successfully processed
std::unordered_set<int> rcv_msg_ids;

// queue of all messages to send
static std::queue<std::string> messages_to_send;

void comm_init(){
  // ensure tcpip buffer is clear
  for (int i = 0; i < MAX_BUFFER; i++) tcpip_buffer[i] = 0;
}

/* Comm entry point. Creates new threads to update and report status 
   and for tcp/ip server. */
void comm_startup()
{
  std::cout << "Starting auto-report loop." << std::endl;
  std::thread update_thread (auto_update_status);
  update_thread.detach();

  std::cout << "Starting loop to send and receive messages." << std::endl;
  std::thread msg_server (message_server);
  msg_server.detach();

  std::cout << "Starting tcpip server." << std::endl;
  std::thread tcpip_th (start_tcpip_server);
  tcpip_th.detach();
}

// tcpip server entry point
void start_tcpip_server()
{
  int server_status;
  while(true)
  {
    server_status = run_server();
    if (server_status)
    {
      create_log("Error running tcpip server. Restarting.");
    }
  }
  
}

// periodically sends a status report based on the report_frequency
void auto_update_status()
{
  // update the status every
  while(true)
    {
      std::this_thread::sleep_for(std::chrono::seconds(report_frequency));
      send_status_report();
    }
}

void send_status_report()
{
  Loc_Value location = eng_get_latest_location();

  log_and_send("hello"); // need Nav to include ID for status report
}

// Changes the report frequency to the provided number of seconds.
void change_report_frequency(int seconds)
{
  report_frequency = seconds;
}

// checks every second to see if there are any messages to send.  If there are messages
// to send will send until the queue is empty. 
void message_server() 
{  
  char buffer[MAX_BUFFER];
  for (int i = 0; i < MAX_BUFFER; i++) buffer[i] = 0;

  while(true)
    {
      std::this_thread::sleep_for(std::chrono::seconds(1)); 
      int rcvd_size = get_buffer(buffer, MAX_BUFFER);
      if ( rcvd_size> 0)
      {
        process_command_buffer(buffer, rcvd_size);
        for (int i = 0; i < MAX_BUFFER; i++) buffer[i] = 0;
      }

      int resend_delay = 5;
      while (!tcpip_enabled && messages_to_send.size())
	    { 
	      if (send_message(messages_to_send.front()))
	      {
	        messages_to_send.pop();
	      }
	    }
    }
}


// sets the tcpip mode by the server
void enable_tcpip_mode(int using_tcpip)
{
  tcpip_enabled = using_tcpip;
}


// copy data over to tcpip_buffer to be processed
void copy_rcvd_data(char *data, int num_bytes_to_copy)
{
  for(int i = 0; i < num_bytes_to_copy; i++) tcpip_buffer[i] = data[i];
  
  process_command_buffer(tcpip_buffer, num_bytes_to_copy);
}

/* this is used to process both APRS and TCPIP commands stored in the provided buffer
 * The passed total_buffer_bytes is the number of total bytes used in the buffer, and the buffer
 * may contain multiple commands.
 *
 * Each command is preceded with the number of bytes of the buffer used for that command */
void process_command_buffer(char *buffer, int total_buffer_bytes)
{
  int bytes_processed = 0;

  while(bytes_processed < total_buffer_bytes)
  {
    uint8_t command_size = buffer[bytes_processed];

    std::stringstream commandstream;
    char get_or_set;
    Command command;
    int command_code;
    std::string rcvd_msg_id; //substring with '{' followed by msg id
    int id; // the extracted msg id
    
    std::string new_command = std::string(buffer + bytes_processed + 1, command_size);

    std::cout << "processing command: " << new_command << "of size " << (int)command_size << std::endl;

    commandstream << new_command;

    commandstream >> get_or_set;

    switch(get_or_set)
    {
      case 's':
      case 'S':
      {
        std::cout << "\tThis is a set command." << std::endl;
        commandstream >> command_code;
        Commands value = static_cast<Commands>(command_code);
        command.command_code = value;

	      switch(command.command_code)         
        {
			    case CMD_Capt_Control_Mode:
				    commandstream >> command.i1_value >> rcvd_msg_id;
				    std::cout << "setting captain mode to " << command.i1_value << std::endl;
            if (rcvd_msg_id[0] != '{')
            {
              std::cout << rcvd_msg_id << "is not a valid message id." << std::endl;
              break;
            }
            id = stoi(rcvd_msg_id.substr(1));

            std::cout << "msg id is: " << id << std::endl;

				    if (id > 0)
            {
                
            }
				    break;

			  case CMD_Eng_Mode:            
				  commandstream >> command.i1_value;
				  std::cout << "setting engineer mode to " << command.i1_value << std::endl;
				  cap_execute_command(command);
				  break;
		 
			  case CMD_Motor_Both:
				  commandstream >> command.d1_value;
				  std::cout << "setting both motors to " << command.d1_value << std::endl;
				  cap_execute_command(command);
				  break; 

			  case CMD_Motor_S:
				  commandstream >> command.d1_value;
				  std::cout << "setting starboard motor to " << command.d1_value << std::endl;
				  cap_execute_command(command);
				  break; 

			  case CMD_Motor_P:
				  commandstream >> command.d1_value;
				  std::cout << "setting port motor to " << command.d1_value << std::endl;
				  cap_execute_command(command);
				  break; 
				  
			  case CMD_Servo_R:
				  commandstream >> command.d1_value;
				  std::cout << "setting rudder to " << command.d1_value << std::endl;
				  cap_execute_command(command);
				  break; 

			  case CMD_Pump:
				  commandstream >> command.d1_value;
				  std::cout << "setting pump to " << command.d1_value << std::endl;
				  cap_execute_command(command);
				  break; 

			  case CMD_Lights:
				  commandstream >> command.d1_value;
				  std::cout << "setting lights to " << command.d1_value << std::endl;
				  cap_execute_command(command);
				  break; 

			  case CMD_Heading:
				  commandstream >> command.d1_value;
				  std::cout << "setting heading to " << command.d1_value << std::endl;
				  cap_execute_command(command);
				  break; 

			  case CMD_Eng_Panic:
				  std::cout << "turning engineer panic on." << std::endl;
				  cap_execute_command(command);
				  break; 
		 
			  case CMD_Nav_Mode:            
				  commandstream >> command.i1_value;
				  std::cout << "setting navigator mode to " << command.i1_value << std::endl;
				  cap_execute_command(command);
				  break;
				
			  case CMD_Add_Waypoint:
				  commandstream >> command.i1_value;
				  commandstream >> command.i2_value;
				  commandstream >> command.d1_value;
				  commandstream >> command.d2_value;
				  std::cout << "appending waypoint at index " << command.i1_value << ", radius " << command.i2_value << 
						  ", longitude " << command.d1_value << ", and latitude " << command.d2_value << std::endl;
				  cap_execute_command(command);
				  break;

			  case CMD_Insert_Waypoint:
				  commandstream >> command.i1_value;
				  commandstream >> command.i2_value;
				  commandstream >> command.d1_value;
				  commandstream >> command.d2_value;
				  std::cout << "inserting waypoint at index " << command.i1_value << ", radius " << command.i2_value << 
						  ", longitude " << command.d1_value << ", and latitude " << command.d2_value << std::endl;
				  cap_execute_command(command);
				  break;
			  } 
		  }
			break;

      case 'g':  //not yet implemented
      case 'G':
        break;
    } 

    bytes_processed += command_size + 1;
  } 

}


int get_messages_to_send(char *data, int buffer_size)
{
  if (messages_to_send.size())
  {
    int bytes_copied = 0, index = 0;
    do{

    } while (bytes_copied + messages_to_send.front().size() < buffer_size);

    return 1;
  }

  // there are no messages to send
  return 0;
}



// creates a log of the provided message.  Logs are contained at ../logs/ and
// will have a filename in the format MMM_DD_YYYY.log 
void create_log(const std::string &message){
  std::time_t time = std::time(nullptr);
  
  char filename[24];
  char timestamp[9];
  
  std::strftime(filename, sizeof(filename), "../logs/%b_%d_%Y.log", std::gmtime(&time));
  std::strftime(timestamp, sizeof(timestamp), "%T", std::gmtime(&time));

  // ensure that only one process can write to the file at a time
  std::lock_guard<std::mutex> guard(log_mtx);
  
  std::ofstream file;

  file.open(filename, std::ios_base::app);
  file << timestamp << " GMT:\n\t" << message << 
       "\n\n---------------------------------------" << std::endl;
  file.close();
}


// Used to create a log as well as sending the contents as a message. Note that messages
// are sent out in 67 character segments if longer than 67 characters.  The characters '~',
// '|', and '{' are reserved and will be removed from the message if found.
// The send portion of this is asynchronous and will return once the message is logged.
void log_and_send(const std::string &message)
{
  create_log(message);

  std::string msg_to_send = remove_reserved_chars(message);

  // Add mesages in 67 char segments. Reserved characters are removed before adding.
  while (msg_to_send.size() > 67) 
    {
      messages_to_send.push(msg_to_send.substr(0,67));
      msg_to_send.erase(0,67);
    }

  // if the remaining size is non-zero, add it to the queue
  if (msg_to_send.size())
    {
      messages_to_send.push(msg_to_send);
    }

}

// helper function to remove APRS reserved characters
// removes '~', '|', and '{'
std::string remove_reserved_chars(const std::string &s)
{
  std::string return_s = s;
  return_s.erase(std::remove(return_s.begin(), return_s.end(), '~'), return_s.end());
  return_s.erase(std::remove(return_s.begin(), return_s.end(), '|'), return_s.end());
  return_s.erase(std::remove(return_s.begin(), return_s.end(), '{'), return_s.end());

  return return_s;
}

// redimentary aprs message
bool send_message(std::string message)
{
  std::string msg = message;
  msg += "{";
  msg += xmt_message_id;

  char *aprs_msg = new char [msg.length() + 1];
  std::strcpy (aprs_msg, msg.c_str());

  aprs_send(aprs_msg);

  xmt_message_id++;

  delete []aprs_msg;
  // TODO implement ack check
  return true;
}


