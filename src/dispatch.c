#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dispatch.h"

struct Event_Data {
  size_t event_arg_size;
  void** handlers;
};

struct Event_Data *events;
int events_size;
int events_count;

void dispatch_init(){
  events_count = 0;
  events_size = 0;
}

EVENT_HANDLE_t dispatch_create_event(size_t event_arg_size){

  events_count++;

  if(events_count > events_size){
    int newsize = events_size * 2;
    struct Event_Data* newlist = (struct Event_Data*) malloc(sizeof(struct Event_Data)*newsize);
    memcpy(newlist,events,sizeof(struct Event_Data)*events_size);
  }

  return (EVENT_HANDLE_t) events_count;
}
