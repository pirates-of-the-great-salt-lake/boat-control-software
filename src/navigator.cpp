#include <cstddef>
#include <iostream>
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif
#include <pthread.h>
#define _USE_MATH_DEFINES
#include <stdlib.h>
#include <math.h>
#include <atomic>
#include "navigator.h"
#include "engineer.h"

using namespace std;

enum Nav_Mode nMode;
enum NavPoint_Event path;
int nav_pt_index;
double speed;
int power;

std::atomic<bool> done(true);
bool isRecovering;
pthread_t th;

struct nav_point* head;

void nav_init()
{
	nMode = Off;
	head = NULL;
	done = true;
	nav_pt_index = 0;
	speed = 20;
	if (access("./config/navstate.bin", F_OK) != -1)
		isRecovering = true;
	else
		isRecovering = false;	
}

static void* nav_thread(void* argv)
{
	cout << "In thread...\n";
	if (head == NULL)
	{
		cout << "Navigation list empty, navigation stopping ...\n";
		return (void *) 1;
	}
	path = PathActive;
	struct nav_point* current = head;
 target_mode_exit:
	while (nMode != Off)
	{
	  switch (nMode)
	  {
	  case Waypoint_Path:
	    while (current->next != NULL || current->status == Active)
	    {
			cout << "Waypoint_Path...\n";
			if (current->status == Active)
			{
				current->status = Target;
				nav_savestate();
				bool reached = false;
				nav_print_path();
				while (!reached)
				{
					eng_output_set_val(Motor_Both, speed);
					Loc_Value eng_loc = eng_get_latest_location();
					point c_loc = { eng_loc.lat, eng_loc.lon }; // Get current location from engineer
					double heading = eng_loc.mag_yaw;
					double newHeading = bearing(c_loc, current->location);
					cout << "Current heading: " << heading << "\n";
					cout << "Correct heading: " << newHeading << "\n";
					cout << "Current Loc: " << c_loc.x << ", " << c_loc.y << "\n"; 
					cout << "D:" << distance(current->location,c_loc) << "\n";
					if(fabs(newHeading - heading) > 30)
					{
						eng_set_heading(newHeading);
						sleep(5);
					}
					if(nMode == Waypoint_Target)
					  goto path_mode_exit;

					// Send new heading to engineer
					eng_set_heading(newHeading);

	                                sleep(1);
					if (distance(current->location, c_loc) < current->radius) // within 10m
					{
						cout << "Reached location" << std::endl;
						cout << "Distance when reached:" << distance(current->location,c_loc)  << std::endl;
						printf("X:%f,Y:%f\n",c_loc.x,c_loc.y);
						printf("Target: X:%f,Y:%f\n",current->location.x,current->location.y);
						reached = true;
					}
				}
				if (current->next != NULL)
				{
					current->status = Completed;
					nav_print_path();
					nav_savestate();
				}
			}
			if (current->next != NULL)
			  current = current->next;
	    }
	    path = PathCompleted;
	  path_mode_exit:
	    nMode = Waypoint_Target;
	  case Waypoint_Target: 
	    while (current->next != NULL || current->status == Target)
	    {
			cout << "Waypoint_Target...\n";
			if (current->status == Target)
			{
				while (1)
				{
					eng_output_set_val(Motor_Both, speed);
				        Loc_Value eng_loc = eng_get_latest_location();
					point c_loc = { eng_loc.lat, eng_loc.lon }; // Get current location from engineer
					double heading = eng_loc.mag_yaw;
					double newHeading = bearing(c_loc, current->location);
					cout << "Current heading: " << heading << "\n";
					cout << "Correct heading: " << newHeading << "\n";
					cout << "D:" << distance(current->location,c_loc) << "\n";
					
					if (fabs(newHeading - heading) > 30)
					{
					  eng_set_heading(newHeading);
					  sleep(5);
					}
					// Send bearing and power to engineer
					if (nMode == Waypoint_Path)
					  goto target_mode_exit;
					sleep(1);
					eng_set_heading(newHeading);
					
					if (distance(current->location, c_loc) < 10)
					{
						// turn off engine to save battery
						eng_output_set_val(Motor_Both, 0);	
						cout << "Holding..." << endl;
						sleep(3);
					}
				}
			}
			current = current->next;
	    }
	    break;
	  default:
	    	eng_output_set_val(Motor_Both, 0);
		cout << "Navigator is in wrong mode, shutting down thread...\n";
		break;
	  }
	}
	return (void *) 1;
}

void nav_startup(int wait_for_completion)
{
	if (done == false)
		pthread_cancel(th);
	done = false;
	if (isRecovering)
	{
		nav_loadstate();
		isRecovering = false;
	}
	if (nMode == Off)
	{
		cout << "Cannot start: Navigation is in off mode!\n";
		return;
	}
	cout << "Started!\n";
	eng_set_mode(Autonomous);
	pthread_create(&th, NULL, nav_thread, NULL);
	if(wait_for_completion){
		pthread_join(th,NULL);
	}
}

double toRad (double degree)
{
        double ratio = (M_PI) / 180;
	return (ratio * degree);
}

double distance(point p1, point p2)
{
	double lat1 = toRad(p1.x);
	double lat2 = toRad(p2.x);
	double lon1 = toRad(p1.y);
	double lon2 = toRad(p2.y);

        double dlong = lon2 - lon1;
	double dlat = lat2 - lat1;

	double dist = pow(sin(dlat / 2), 2) + cos(lat1) * cos(lat2) * pow(sin(dlong / 2), 2);
	dist = 2 * asin(sqrt(dist));
	
	double eRadius = 6371000; 
	return fabs(dist * eRadius);
}

double bearing(point p1, point p2)
{

	double lat1 = toRad(p1.x);
	double lat2 = toRad(p2.x);
	double lon1 = toRad(p1.y);
	double lon2 = toRad(p2.y);
	
	double dLong = lon2 - lon1;

	double lon = sin(dLong) * cos(lat1);
	double lat = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLong);

	double brng = atan2(lon, lat);

	brng = brng * (180.0 / M_PI);
	brng = (int)(brng + 360) % 360;

	return brng;
}

void nav_deinit()
{
	nMode = Off;
	done = true;
	head = NULL;
	nav_pt_index = 0;
	speed = 0;
	if (remove("./config/navstate.bin") == 0)
		cout << "Navigator state file deleted successfully!\n";
	else
		cout << "Warning! Navigator state file was not deleted.\n";
	eng_output_set_val(Motor_Both, speed);
}

void nav_set_mode(enum Nav_Mode mode)
{
	switch (mode)
	{
	case Off:
		nMode = Off;
		break;
	case Waypoint_Path:
		nMode = Waypoint_Path;
		// Start getting points then send heading and speed to engineer.
		break;
	case Waypoint_Target:
		nMode = Waypoint_Target;
		// Hold current position then send heading and speed to engineer
		break;
	default:
		break;
	}

	return;
}

enum Nav_Mode nav_get_mode (void)
{
	return nMode;
}

void nav_set_optionf(enum Nav_FOption opt, float val)
{
	switch (opt)
	{
	case Max_Speed:
		speed = static_cast<double>(val);
		break;
	case Max_Power:
		power = static_cast<int>(val);
		break;
	default:
		break;
	}

	return;
}

void nav_set_optioni(enum Nav_IOption opt, int val)
{
	switch (opt)
	{
	default:
		break;
	}

	return;
}

void nav_shutdown()
{
	if (!done)
		pthread_cancel(th);
	nMode = Off;
	done = true;
	if (remove("./config/navstate.bin") == 0)
		cout << "Navigator state file deleted successfully!\n";
	else
		cout << "Warning! Navigator state file was not deleted.\n";
	eng_output_set_val(Motor_Both, 0);
}

void nav_path_add_path(nav_point* point)
{
        nav_path_insert_path(nav_pt_index, point);
}

void nav_path_insert_path(int val, nav_point* point)
{
	if (isRecovering)
		return;
	// Return if attempting to insert at negative value
	if (val < 0)
	{
		cout << "Error! Inserting at position that is negative.\n";
		return;
	}
		
	struct nav_point* last = head;
	struct nav_point* temp = point;
	// Ensure that all the points being passed in have a reasonable radius, if not set to 15m as default
	if(temp->radius <= 0 || temp->radius > 200)
	{
		cout << "Setting point " << temp->indx << " radius to default value of 15m.\n";
		temp->radius = 15;
	}
	while (temp->next != NULL)
	{
		
		if(temp->radius <= 0 || temp->radius > 200)
		{
			cout << "Setting point " << temp->indx << " radius to default value of 15m.\n";
			temp->radius = 15;
		}
		temp = temp->next;
	}
	// If the link list we have is empty or we are inserting at the start of the list
	if (head == NULL || val == 0)
	{
	        nav_pt_index = 0;
		point->prev = NULL;
		point->status = Active;
		point->indx = nav_pt_index;
		// Dynamically allocate new point
		nav_point* insert_point = (nav_point*) malloc(sizeof(nav_point)); 
		*insert_point=*point;
		head = insert_point;
		nav_pt_index++;
		
		while (point->next != NULL)
		{
		      nav_point* next_point = (nav_point*) malloc(sizeof(nav_point));
		      *next_point = *(point->next); 
		      next_point->prev = insert_point;
		      insert_point->next = next_point;
		      point = point->next;
		      next_point->status = Active;
		      next_point->indx = nav_pt_index;
		      insert_point = next_point;
		      nav_pt_index++;
		}
		// Add the reset of the linked list when inserting at indx 0	
		if (last != NULL)
		{
		      last->prev = insert_point;
		      insert_point->next = last;
		      last->indx = nav_pt_index;
		      nav_pt_index++;
		      while (last->next != NULL)
		      {
			    last = last->next;
			    last->indx = nav_pt_index;
			    nav_pt_index++;
		      }
		}
		nav_savestate();	
		return;
	}

	int count = 1;

	while (count != val)
	{
		if (last->next == NULL)
		{	
			nav_point* insert_point = (nav_point*) malloc(sizeof(nav_point)); 
			*insert_point = *point;
			last->next = insert_point;
			insert_point->prev = last;
			insert_point->indx = nav_pt_index;
			nav_pt_index++;

			while (point->next != NULL)
			{
		      		nav_point* next_point = (nav_point*) malloc(sizeof(nav_point));
		      		*next_point = *(point->next); 
		      		next_point->prev = insert_point;
		      		insert_point->next = next_point;
		      		point = point->next;
		      		next_point->status = Active;
		      		next_point->indx = nav_pt_index;
		      		insert_point = next_point;
		      		nav_pt_index++;
			}
			cout << "Warning! The position inserting to is greater than the list, new point added to the end of the list.\n";
			nav_savestate();	
			return;
		}

		last = last->next;
		count++;
	}
	if (last->next == NULL)
	{
		
		nav_point* insert_point = (nav_point*) malloc(sizeof(nav_point)); 
		*insert_point = *point;
		last->next = insert_point;
		insert_point->prev = last;
		insert_point->indx = nav_pt_index;
		nav_pt_index++;
		while (point->next != NULL)
		{
		      nav_point* next_point = (nav_point*) malloc(sizeof(nav_point));
		      *next_point = *(point->next); 
		      next_point->prev = insert_point;
		      insert_point->next = next_point;
		      point = point->next;
		      next_point->status = Active;
		      next_point->indx = nav_pt_index;
		      insert_point = next_point;
		      nav_pt_index++;
		}
		nav_savestate();	
		return;
	}
	temp = last->next;
	nav_pt_index = temp->indx;
	point->indx = nav_pt_index;
	nav_point* insert_point = (nav_point*) malloc(sizeof(nav_point)); 
	*insert_point = *point;
	last->next = insert_point;
	insert_point->prev = last;
	while (point->next != NULL)
	{
		nav_point* next_point = (nav_point*) malloc(sizeof(nav_point));
		*next_point = *(point->next); 
		next_point->prev = insert_point;
		insert_point->next = next_point;
		point = point->next;
		next_point->status = Active;
		next_point->indx = nav_pt_index;
		insert_point = next_point;
		nav_pt_index++;
	}
	insert_point->next = temp;
	temp->prev = insert_point;
	nav_pt_index++;
	temp->indx = nav_pt_index;
	while(temp->next != NULL)
	{
	        temp = temp->next;
		nav_pt_index++;
		temp->indx = nav_pt_index;
	}
	nav_pt_index++;
	nav_savestate();	
	return;
}

nav_point* nav_get_path()
{
	struct nav_point* last = head;
	if (last == NULL)
		return NULL;
	while (last->next != NULL)
	{
		if (last->status == Target)
			break;
		else
			last = last->next;
	}

	return last->status == Target ? last : NULL;
}

nav_point** nav_get_abandoned_navpoints()
{
	struct nav_point** ptr = NULL;
	struct nav_point* last = head;

	while (last != NULL)
	{
		if (last->status == Abandoned) // ?
			ptr = &last;
	}
	return ptr;
}

void nav_clear_abandoned_navpoints()
{
	if (head == NULL)
		return;

	struct nav_point* temp;

	if (head->status == Abandoned)
	{
		temp = head;
		head = head->next;
		head->prev = NULL;
		free(temp);
	}

	struct nav_point* last = new struct nav_point;
	last = head;

	while (last->next != NULL)
	{
		if (last->next->status == Abandoned)
		{
			temp = last->next;
			last->next = last->next->next;
			if (last->next != NULL)
				last->next->prev = last;
			free(temp);
		}
		else
			last = last->next;
	}
	return;
}

void nav_clear_completed_navpoints()
{
	if (head == NULL)
		return;

	struct nav_point* temp;

	if (head->status == Completed)
	{
		temp = head;
		head = head->next;
		head->prev = NULL;
		free(temp);
	}

	struct nav_point* last = head;

	while (last->next != NULL)
	{
		if (last->next->status == Completed)
		{
			temp = last->next;
			last->next = last->next->next;
			if (last->next != NULL)
				last->next->prev = last;
			free(temp);
		}
		else
			last = last->next;
	}
	return;
}

void nav_clear_all_navpoints()
{
	cout << "Clearing & freeing all the points from the navigator list.\n";
	if (head == NULL)
		return;

	struct nav_point* last = head;
	struct nav_point* temp;
	nav_pt_index = 0;

	while (last->next != NULL)
	{
		temp = last->next;
		free(last);
		last = temp;
	}
	free(last);
	head = NULL;

	return;
}

void nav_savestate()
{
	cout << "Saving the navigator state to file...\n";
	FILE *pFile;
	pFile = fopen("./config/navstate.bin", "wb");

	if (pFile != NULL)
	{
		struct nav_point* current = head;
		struct nav_point* next = NULL;
		struct nav_point* prev = NULL;
		
		while (current != NULL)
		{
			next = current->next;
			prev = current->prev;		
			
			current->next = NULL;
			current->prev = NULL;

			fseek(pFile, 0, SEEK_END);
			fwrite(current, sizeof(nav_point), 1, pFile);
			
			current->next = next;
			current->prev = prev;
			
			next = NULL;
			prev = NULL;			
			
			current = current->next;
		}
		fclose(pFile);
		pFile = NULL;
	} 
	else 
	{
		cout << "Error opening navstate file!\n";
	}
	return;
}

void nav_loadstate()
{
	cout << "Loading the navigator state from file...\n";
	FILE *pFile;
	pFile = fopen("./config/navstate.bin", "rb");
	
	if (pFile != NULL)
	{
		nav_point* start = NULL;
	
		fseek(pFile, 0, SEEK_END);
		long fSize = ftell(pFile);
		rewind(pFile);
			
		int points_num = (int) (fSize / (sizeof(nav_point)));
		int i = 0;
		for (; i < points_num; ++i)
		{
			fseek(pFile, (sizeof(nav_point) * i), SEEK_SET);
			start = nav_loadnext(start, pFile);  
		}
	}
	else
	{
		cout << "Error opening navstate file!\n";
	}	
}

Nav_Point* nav_loadnext(nav_point* start, FILE* pFile)
{
	size_t returnVal;
	if (start == NULL)
	{
		start = (nav_point*) malloc(sizeof(nav_point));
		returnVal = fread(start, sizeof(nav_point), 1, pFile);
		start->next = NULL;
		start->prev = NULL;
		head = start;	
	}
	else
	{
		nav_point* indxPt = start;
		nav_point* newPt = (nav_point*) malloc(sizeof(nav_point));
		while (indxPt->next != NULL)
		{
			indxPt = indxPt->next;
		}
		returnVal = fread(newPt, sizeof(nav_point), 1, pFile);
		indxPt->next = newPt;
		newPt->next = NULL;
		newPt->prev = indxPt;			
	}
	return start;
}

void nav_print_path()
{
	if (head == NULL)
	{
		cout << "The navigation point list is empty.\n";
		return;
	}

	struct nav_point* last = head;

	cout << "HEAD --> ";

	while (last != NULL)
	{
   
		cout << last->indx << " | X:" << last->location.x << ",Y:" << last->location.y 
			<< " (" << last->radius << "m) | ";
		if (last->status == 1)
		  cout << "Active --> ";
		else if (last->status == 2)
		  cout << "Target --> ";
		else if (last->status == 3)
		  cout << "Completed --> ";
		else
		  cout << "Abandoned --> ";
		if (last->next == NULL)
		  cout << "END\n";

		last = last->next;
	}

	return;
}
