#include "aprs.h"
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#define AGWSIZE 47

struct agwpe_s { 
  short portx;   /* 0 for first, 1 for second, etc. */
  short port_hi_reserved; 
  short kind_lo;  /* message type */
  short kind_hi;
  char call_from[10];
  char call_to[10];
  int data_len;   /* Number of data bytes following. */
  int user_reserved;
};

static const struct agwpe_s emptyPack = { 0, 0,0,0, 0, 0, 0, 0};

const char * callsign = "KJ7MKE-7\0";
const char * destc = "APZ999\0\0\0";
const char * desta = "WIDE2-2\0\0";

char buffer[8192];
int bufCursor = 0;

int sockfd = 0;

int open_socket()
{
    int newSockfd = 0;
    struct sockaddr_in serv_addr;


    if((newSockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Error : Could not create socket \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(8010);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        return -1;
    }

    if( connect(newSockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       printf("\n Error : Connect Failed \n");
       return -1;
    }


    return newSockfd;
}

void read_loop(){
  printf("APRS Read loop started.\n");
  struct agwpe_s mon_cmd;

  int runloop_sockfd = sockfd;
  memset (&mon_cmd, 0, sizeof(mon_cmd));
  mon_cmd.kind_lo = 'm';
  send(runloop_sockfd,&mon_cmd,sizeof(mon_cmd),0);

  while(1){
    char packBuffer[1024];

    printf("Waiting for data.\n");
    int numRead = recv(runloop_sockfd, packBuffer, 1024, NULL);
    packBuffer[numRead] = 0;
    printf("Data read from direwolf.\n");
    if(numRead < 0){
	printf("Coudn't read from socket\n");
    }
    fflush(stdout);
    char* msg_start = strchr(packBuffer + 37,0x0d);
    if(msg_start == NULL)
      continue;
    msg_start = strchr(msg_start+1,':');
    if(msg_start == NULL)
      continue;
    msg_start += 1;
    char* msg_end = msg_start + 9;
    printf("Checking if packet is in message format.\n");
    if(numRead > 0 && *msg_end==':') {
      printf("Message received for: %.9s\n",msg_start);
      if(!strncmp(msg_start,callsign,8)){
        int len = numRead-(msg_start-packBuffer)-3;
        printf("Message was meant for us! Message len: %d\n",len);
	memcpy(buffer+bufCursor,msg_start,len);
	bufCursor += len;
	printf("Contents of buffer: %s\n",buffer);
      }
    }
    printf("Done with aprs data handling.\n");
    fflush(stdout);
  }
}

int get_buffer(char *data, int buffer_size){
  memcpy(data,buffer,bufCursor);
  int ret = bufCursor;
  bufCursor = 0;
  return ret;
}

void close_socket(int fd){
  close(fd);
}

void get_packet(struct agwpe_s * dest, int dataLen){
  dest->kind_lo = 'V';
  dest->data_len = dataLen + 11;
  memcpy(dest->call_from,callsign,9);
  memcpy(dest->call_to,destc,9);
  *((char*)dest+36) = 1;
  memcpy(dest+37,desta,9);

}

int aprs_send(char* data){
  //char * data = "=4044.10N/11212.83WxCustom.Test longer.";
  char buf[500];

  get_packet((struct agwpe_s*)buf, strlen(data));

  memcpy(buf + AGWSIZE, data, strlen(data));


  printf("Sending data with total len: %d\n",AGWSIZE + strlen(data));

  /*for(int i=0; i < AGWSIZE + strlen(data);i+=2){
    printf("%02X%02X \t",buf[i],buf[i+1]);
    fwrite(buf+i,1,2,stdout);
    printf("\n");
  }*/

  int status = send(sockfd, buf, AGWSIZE + strlen(data), 0);
  if(status < 0){
    printf("Error sending. Errno: %d\n",errno);
  }

}

void run_direwolf(){
  char *args[] = {"direwolf", "-t", "0", "-d", "a", "-q hd","-c", "/root/direwolf.conf", 0}; /* each element represents a command line argument */
  char *env[] = { 0 };
  system("killall direwolf");
  system("config-pin P9_11 gpio");
  system("echo 30 > /sys/class/gpio/unexport");
  execve("/usr/bin/direwolf",args,env);
}

void aprs_shutdown(){
    close_socket(sockfd);
}

void aprs_init(){
  sockfd = open_socket();
  while(sockfd == -1){
    if(fork() == 0){
      std::cout << "Direwolf wasn't responding, killing and restarting..." << std::endl;
      run_direwolf();
    }
    sleep(2);
    sockfd = open_socket();
  }
  if(sockfd != -1){
    printf("APRS connected to direwolf.\n");
  }
  if(fork() == 0){
    printf("Starting APRS read loop.\n");
    read_loop();
  }
  if(sockfd == 0){
    printf("Error error connecting to direwolf!");
  }
}

