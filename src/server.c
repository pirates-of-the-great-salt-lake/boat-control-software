#include <stdio.h> 
#include <unistd.h>
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <arpa/inet.h>
#include "comms.h"

#define MAX_BUFFER 1024
#define PORT 9000
#define SA struct sockaddr

// handles one connection
void read_and_send(int sock_fd)
{
    char buffer[MAX_BUFFER];
    bzero(buffer, MAX_BUFFER);
    ssize_t num_bytes_rcvd, num_bytes_sent;

    // keep communicationing while connected
    while(1)
    {        
        num_bytes_rcvd = read(sock_fd, buffer, sizeof(buffer));
        if (num_bytes_rcvd < 1) 
        {
            printf("The client has closed or disconnected during a read.\n");
            break;
        }   
        printf("Read %d bytes from tcpip client.\n", (int)num_bytes_rcvd);
        printf("Received: %s\n", buffer);

        // copy the command data to be processed
        copy_rcvd_data(buffer, num_bytes_rcvd);
        bzero(buffer, MAX_BUFFER);

        // send messages
        int message_to_send = get_messages_to_send(buffer, MAX_BUFFER);

        if (message_to_send)
        {
            num_bytes_sent = write(sock_fd, buffer, sizeof(buffer)); 
            if (num_bytes_sent < 0) 
            {
                printf("Failed to send to client - connection broken.");
                break;   
            }
            printf("Sent %d bytes to tcpip client.", (int)num_bytes_sent);  
            printf("Sent: %s\n", buffer);  
            bzero(buffer, MAX_BUFFER);
        }

    }

    // the connection was closed
    enable_tcpip_mode(0);
    close(sock_fd);
}


int run_server()
{
    int server_sock_fd, conn_sock_fd;
    socklen_t client_length;
    struct sockaddr_in serv_addr, client_addr;

    server_sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sock_fd == -1) 
    {
        printf("Failed to create socket.\n");
        return -1;
    }
    else printf("Socket created.\n");

    // set up server info
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("0.0.0.0");
    serv_addr.sin_port = PORT;

    if (bind(server_sock_fd, (SA*) &serv_addr, sizeof(serv_addr)) != 0)
    {
        printf("socket bind failed.\n");
        return -1;
    }
    else printf("Socket bound.\n");

    if ((listen(server_sock_fd, 10)) != 0)
    {
        printf("Listen failed.\n");
        return -1;
    }
    else printf("Server is listening.\n");

    // server keeps running and making connections as necessary
    while(1)
	{
        client_length = sizeof(client_addr);

        conn_sock_fd = accept(server_sock_fd, (SA*) &client_addr, &client_length);
        if (conn_sock_fd < 0)
        {
            printf("Server failed to accept client.\n");
            return -1;
        }
        else printf("Server accepted client.\n");

        if (!fork()){
            close(server_sock_fd); // the child/client connection doesn't need the listen socket
            //TODO setup socket keepalive options
            enable_tcpip_mode(1);
            read_and_send(conn_sock_fd);
        }
        else close(conn_sock_fd); // the parent/server doesn't need the client connection socket
    }

    return 0;
}
