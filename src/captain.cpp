#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <string>
#include "engineer.h"
#include "navigator.h"
#include "captain.h"
#include "aprs.h"

#ifndef MAX_MOTOR_SPEED
#define MAX_MOTOR_SPEED 100
#endif
pthread_t eng_th, nav_th, comms_th;
power_info_t power_info; 

void startup(void){
  eng_startup(Manual);
  comm_startup();
  //nav_startup();
}

void run_init(void){
  printf("Initializing engineer...");
  eng_init();
  printf("Engineer initialized.");
  comm_init();
  nav_init();
  aprs_init();
}

void cap_set_mode(Control_Mode mode){
  
}

void cap_test_nav(){
	nav_init();
	Nav_Point a, b, c, d, e, f, g, h, i;
	std::cout << "Adding points for figure 8 test\n";
	a = { {40.710537, -111.946773}, NULL, NULL, Active, 10 };
	b = { {40.710728, -111.947469}, NULL, NULL, Active, 10 };
	c = { {40.711058, -111.946960}, NULL, NULL, Active, 10 };
	d = { {40.711518, -111.946634}, NULL, NULL, Active, 10 };
	e = { {40.711706, -111.947317}, NULL, NULL, Active, 10 };
	f = { {40.711350, -111.947727}, NULL, NULL, Active, 10 };
	g = { {40.711058, -111.946960}, NULL, NULL, Active, 10 };
	h = { {40.710779, -111.946530}, NULL, NULL, Active, 10 };
	i = { {40.710491, -111.946174}, NULL, NULL, Active, 10 };

	nav_path_add_path(&a);
	nav_path_insert_path(1,&b);
	nav_path_insert_path(2,&c);
	nav_path_insert_path(3,&d);
	nav_path_insert_path(4,&e);
	nav_path_insert_path(5,&f);
	nav_path_insert_path(6,&g);
	nav_path_insert_path(7,&h);
	nav_path_insert_path(8,&i);
	nav_print_path();
	nav_set_mode(Waypoint_Path);
	nav_set_optionf(Max_Speed, MAX_MOTOR_SPEED);
	nav_startup();

}

void do_mag_cal(){
  eng_set_mode(Manual);
  eng_output_set_val(Servo_R,0);
  eng_output_set_val(Motor_Both,MAX_MOTOR_SPEED);
  sleep(3);
  eng_output_set_val(Servo_R,-45);
  eng_do_mag_cal();
  sleep(15);
  eng_end_mag_cal();
}

void check_lights(){
  eng_output_set_val(Lights,power_info.Solar_Voltage < 10);
}

void send_loc(){
  char buff[100];
  Loc_Value location = eng_get_latest_location();
  double lat_mins = abs(location.lat - ((int)location.lat))*60;
  double lon_mins = abs(location.lon - ((int)location.lon))*60;
  snprintf(buff,100,"=%i%2.2fN/%i%2.2fWxDecker Lake Autonomouse Boat Test!Going %f m/s.",(int)location.lat,lat_mins,abs((int)location.lon),lon_mins,location.gps_velocity);
  std::string output = buff;
  log_and_send(output);
}

int main(int argc, char* argv){
  printf("BCSD initializing...\n");
  char cwd[500];
   if (getcwd(cwd, sizeof(cwd)) != NULL) {
       printf("Current working dir: %s\n", cwd);
   }
  run_init();
  printf("BCSD Starting...\n");
  startup();
  printf("BCSD Started...\n");
  send_loc();
  //eng_output_set_val(Motor_Both,MAX_MOTOR_SPEED);
  cap_test_nav();
  int cnt = 0;
  while(1){
    power_info = eng_get_cc_state();  
    printf("Battery voltage: %f\n",power_info.Batt_voltage);
    fflush(stdout);
    check_lights();
    if(cnt % 30 == 0)
      send_loc();
    cnt++;
    sleep(1);
  }
  return 0;
}

bool cap_execute_command(Command command)
{
    bool command_is_valid = true;

    switch(command.command_code)
    {
        /******************************************** CAPTAIN COMMANDS ****************************************************/
        // sets the captain's control mode
        // control mode value stored in command.i1_value;
        // valid values 0-3 (enum)
        case CMD_Capt_Control_Mode:
          cap_set_mode((Control_Mode)command.i1_value);
            break;

        /******************************************** ENGINEER COMMANDS ****************************************************/
        // sets the engineer mode by calling "void eng_set_mode(enum Eng_modes)"
        // mode stored in command.i1_value
        // valid values 0-2 (enum)
        case CMD_Eng_Mode:
          eng_set_mode((Eng_modes)command.i1_value);
            break;

        // sets both motor powers by calling "int eng_output_set_val(enum Eng_OutputDev, double value)"
        // Eng_OutputDev is Motor_Both = 0,
        // value is stored in command.d1_value
        // valid values are -100 to 100
        case CMD_Motor_Both:
          eng_output_set_val(Motor_Both,command.d1_value);
            break;

        // sets the starboard motor power by calling "int eng_output_set_val(enum Eng_OutputDev, double value)"
        // Eng_OutputDev is Motor_S = 1
        // value is stored in command.d1_value
        // valid values are -100 to 100
        case CMD_Motor_S:
          eng_output_set_val(Motor_S,command.d1_value);
            break;

        // sets the port motor power by calling "int eng_output_set_val(enum Eng_OutputDev, double value)"
        // Eng_OutputDev is Motor_P = 2
        // value is stored in command.d1_value
        // valid values are -100 to 100
        case CMD_Motor_P:
          eng_output_set_val(Motor_P,command.d1_value);
            break;

        // sets the rudder servo position by calling "int eng_output_set_val(enum Eng_OutputDev, double value)"
        // Eng_OutputDev is Servo_R = 3
        // value is stored in command.d1_value
        // valid values are -90 to 90
        case CMD_Servo_R:
          eng_output_set_val(Servo_R,command.d1_value);
            break;

        // Control the lights by calling "int eng_output_set_val(enum Eng_OutputDev, double value)"
        // Eng_OutputDev is Pump = 4
        // value is stored in command.d1_value
        // >0 sets pump high, <=0 sets pump low
        case CMD_Pump:
          eng_output_set_val(Pump,command.d1_value);
            break;

        // Control the lights by calling "int eng_output_set_val(enum Eng_OutputDev, double value)"
        // Eng_OutputDev is Lights = 5
        // value is stored in command.d1_value
        // >0 turns on lights, <=0 turns lights off
        case CMD_Lights:
          eng_output_set_val(Lights,command.d1_value);
            break;

        // sets the ships desired heading in degrees by calling "void eng_set_heading(double heading)"
        // value stored in command.d1_value
        // valid range 0-360
        case CMD_Heading:
          eng_set_heading(command.d1_value);
            break;

        // Force the engineer to shut down all peripheral systems in a safe manner by calling "void eng_panic(void)"
        // no values
        case CMD_Eng_Panic:
          eng_panic();
            break;

        /******************************************** NAVIGATOR COMMANDS ****************************************************/
        // sets the navigator mode by calling "void nav_set_mode(enum Nav_Mode mode)"
        // value stored in command.i1_value
        // valid values: 0-2 (enum)
        case CMD_Nav_Mode:

            break;

        /* adds a new waypoint to the end of the linked list by calling "void nav_path_add_path(Nav_Point*)"
         * need to pass a pointer to a Nav_Point struct as follows
         *  typedef struct nav_point {
         *      int id;                         // NOT YET IMPLEMENTED IN NAV - will be stored in command.i1_value
         *      int radius;                     // NOT YET IMPLEMENTED IN NAV - will be stored in command.i2_value
         *      struct point location;          // x (longitude) is stored in command.d1_value, y (latitude) is stored in command.d2_value
         *      struct nav_point* next;
         *      struct nav_point* prev;
         *      enum Nav_Point_Status status;   // use Active = 1
         *  } Nav_Point; */
         case CMD_Add_Waypoint:
            break;

        /* inserts a new waypoint at the specified val into the linked list by calling "void nav_path_insert_path(int val, nav_point* point)"
         *
         * val represents where in the link list to insert the new point, with 0 setting it to the target waypoint
         * val currently does not have a place to be stored in the Command structure. Can potentially add to either id or radius value as thousands
         * for example to add a waypoint at index 8 with waypoint id 56 could set i1_value to 8056 with (int)i1_value/1000 being the insert value
         * and i1_value % 1000 being the waypoint id
         *
         * need to pass a pointer to a Nav_Point struct as follows
         *  typedef struct nav_point {
         *      int id;                         // NOT YET IMPLEMENTED IN NAV - will be stored in command.i1_value
         *      int radius;                     // NOT YET IMPLEMENTED IN NAV - will be stored in command.i2_value
         *      struct point location;          // x (longitude) is stored in command.d1_value, y (latitude) is stored in command.d2_value
         *      struct nav_point* next;
         *      struct nav_point* prev;
         *      enum Nav_Point_Status status;   // use Active = 1
         *  } Nav_Point; */
         case CMD_Insert_Waypoint:
            break;


        default:
            command_is_valid = false;
    }

    return command_is_valid;
}
