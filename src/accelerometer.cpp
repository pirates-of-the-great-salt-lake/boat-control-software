
/*
Device address: 110 100X  with X = pin AD0
Setup:
write to PWR_MGMT_1 at reg address 0110 1011 (0x6B)
    write 0010 1000 :  (not reset, sleep off, cycle on, temp sensor off, internal 8 mhz clock)
    for more timing stability (highly recommended by register map, but needed?) can write 0010 1001
    to use the X-axis gyroscope PLL reference.  
write to PWR_MGMT_2 at reg address 0110 1100 (0x6C)
write 0111 0111 : (wakeup frequency: 5 Hz, standby X,Y turn on Z accel and standby X,Y,Z gyroscope).


Read Accelerometers (16 bit signed)
Z out bits 15:8 located at 0011 1111 (0x3f)
Z out bits 7:0 located at 0100 0000 (0x40)
*/
#include <iobb.h>
#include "accelerometer.h"
static int i2c_handle;
static unsigned char addr = 0x68;


//Will return -1 if write to power reg fails
int accel_init() {
    configure_i2c_pins(19,20);
    i2c_handle = i2c_open(2, addr);

    unsigned char wb[2];
    wb[0] = 0x6B;
    wb[1] = 0x28;

    int error = 0;
    error += i2c_write(i2c_handle, wb, 2);
    
    wb[0] = 0x6C;
    wb[1] = 0x77;
    error += i2c_write(i2c_handle, wb, 2);

    return error;
}

bool accel_is_upsidedown() {
    unsigned char output;
    i2c_write_byte(i2c_handle, 0x3f);
    i2c_read_byte(i2c_handle, &output);

    //check the msb of the z-axis on the accelerometer
    //to see if it's negative
    return !(output & 0x80);
}


