#include "pid.h"
#include <chrono>
#include <stdio.h>


static double out_min, out_max, ig, dg, pg, i_sum, this_target, last_error;
static std::chrono::time_point<std::chrono::high_resolution_clock> last_time, current;

int pid_init(double min, double max, double p_gain, double i_gain, double d_gain) {
    out_min = min;
    out_max = max;
    ig = i_gain;
    pg = p_gain;
    dg = d_gain;

    printf("igain: %f \n", ig);
    printf("p gain: %f \n", pg);
    printf("d gain: %f \n", dg);

    last_error = 0.0;
    last_time = std::chrono::high_resolution_clock::now();
    }

void pid_err_reset() {
    i_sum = 0.0;    
}

void pid_set_target(double target) {
    this_target = target;
}

double pid_get_output(double current_value) {
    double d = 0.0;
    using namespace std::chrono;
    current = high_resolution_clock::now();
    auto difference = duration_cast<duration<double>>(current - last_time);
    
    double error = this_target - current_value;

    #ifdef DEBUG
    printf("error: %f \n", error);
    #endif

    if(error < -180 )
        error = 360 + error;
    else if(error > 180)
        error = error - 360;

    i_sum += ig * error;

    if(difference.count() != 0)
        d = ((error - last_error)/difference.count()) * dg;

    last_error = error;
    last_time = current;

    double new_output = (error * pg); //+ i_sum + d;

    if(new_output > out_max) {
        i_sum = out_max;
        new_output = out_max;
    }
    
    if(new_output < out_min) {
        i_sum = out_min;
        new_output = out_min;
    }


    return new_output * -1; //Needs to be flipped

}
