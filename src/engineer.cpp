/**
 * TODO: do error checking
 * TODO: interface with comms
 * TODO: setup appropriate events
 * */

#define DEBIAN

#ifdef DEBIAN
#define SB_PATH "/sys/class/pwm/pwm-1:0/"
#define PRT_PATH "/sys/class/pwm/pwm-1:1/"
#define RUDDER_PATH "/sys/class/pwm/pwm-4:0/"
#else
#define SB_PATH "/sys/class/pwm/pwmchip0/pwm0/"
#define PRT_PATH "/sys/class/pwm/pwmchip0/pwm1/"
#define RUDDER_PATH "/sys/class/pwm/pwmchip2/pwm0/"
#endif

/** these are all on header 9
 * pin rudder pwm1a 
 * pin 12 hbt 
 * pin 11 pto
 * pin 13 nav light
 * pin 15 pump cutoff (pump off high)
 * pin 16 mag trigger
 * pin 17 water_sense (active high)
 * pin 19 scl
 * pin 20 sda
 * pin 21 pwm0b port motor
 * pin 22 pwm0a strbrd
 * pin 24 tx cc
 * pin 36 rcv
 **/
#define CC_PATH "/dev/ttyUSB0"
#define MAX_FORWARD_DC 1700000
#define MIN_REVERSE_DC 1300000
#define MAX_RUDDER_DC 2600000
#define MIN_RUDDER_DC 450000
#define PWM_CENTER_DC 1500000
#define CONFIG_PATH "config/engineer.conf"

#define LIGHT_PIN 13 //all of these are on pin header 9
#define PUMP_PIN 15
#define WATER_SENSOR_PIN 17
#define HBT_PIN 12

#include "accelerometer.h"
#include "engineer.h"
#include "comms.h"
#include "MLX90393_compass.h"
#include <renogy_cc_addresses.h>
#include <gps.h>
#include <pthread.h>
#include <iobb.h>
#include <iostream>
#include <fstream>
#include <map>
#include <math.h>
#include <modbus/modbus-rtu.h>
#include <modbus/modbus.h>
#include "pid.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>


std::string sb_Path, prt_Path, rudder_Path;
bool is_initialized = false;
enum Eng_modes mode;
FILE *tempfp, *sb_duty_fp, *prt_duty_fp, *rudder_duty_fp ;

std::map<std::string, double> config_map;
std::fstream configf;

modbus_t *mbt;
pthread_t GPS_thread, mag_Thread, daq_thread, heart_beat_thread; //short for data acqusition thread
pthread_mutex_t gps_location_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mag_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t i2c_lock = PTHREAD_MUTEX_INITIALIZER;

volatile Loc_Value current_loc;
struct gps_data_t gps_Stuffs;

enum last_location{compass, gps};
int last_sensor;
Error error_state = {0};

void getGPSdata(struct gps_data_t* data);
void * mag_thread(void *);

bool hb_on;
void * doHeartBeat(void *);
void write_conf();
void* startGPSloop(void *) {
    gps_mainloop(&gps_Stuffs, 50000000, getGPSdata);
}

void eng_init(){

}

int eng_startup(enum Eng_modes new_mode) {
    using namespace std;

    mode = new_mode;
    int error = 0;
    int ret = 0;
    last_sensor = 5; //arbitrary value > 1
    configf.open(CONFIG_PATH);
    if(configf.fail()) {
	    printf("failed to open config file");
    }
    //Begin initiallizing PWM and gpio pins
    #ifdef DEBIAN
    system("config-pin P9_21 pwm"); //port motor
    system("config-pin P9_22 pwm"); //starboard motor
    system("config-pin P9_14 pwm"); //steering servo
    system("config-pin P9_13 gpio"); //steering servo
    system("config-pin P9_15 gpio"); //steering servo
    system("config-pin P9_17 gpio"); //steering servo
    system("config-pin P9_24 uart"); //steering servo
    system("config-pin P9_26 uart"); //steering servo
    #endif

    iolib_init();
    iolib_setdir(9, 11, DigitalOut);
    iolib_setdir(9, LIGHT_PIN, DigitalOut);
    iolib_setdir(9, PUMP_PIN, DigitalOut);
    iolib_setdir(9, HBT_PIN, DigitalOut);
    iolib_setdir(9, WATER_SENSOR_PIN, DigitalIn);

    pin_low(9, LIGHT_PIN);
    pin_low(9, PUMP_PIN);

    sb_Path = SB_PATH;
    prt_Path = PRT_PATH;
    rudder_Path = RUDDER_PATH;

    tempfp = fopen((sb_Path + "period").c_str(), "w");
    fprintf(tempfp, "20000000");
    fclose(tempfp);

    tempfp = fopen((prt_Path + "period").c_str(), "w");
    fprintf(tempfp, "20000000");
    fclose(tempfp);

    tempfp = fopen((rudder_Path + "period").c_str(), "w");
    fprintf(tempfp, "20000000");
    fclose(tempfp);

    sb_duty_fp = fopen((sb_Path + "duty_cycle").c_str(), "w+");
    prt_duty_fp = fopen((prt_Path + "duty_cycle").c_str(), "w+");
    rudder_duty_fp = fopen((rudder_Path + "duty_cycle").c_str(), "w+");

    eng_output_set_val(Motor_Both, 0);
    eng_output_set_val(Servo_R, 0);

    tempfp = fopen((sb_Path + "enable").c_str(), "w");
    fprintf(tempfp, "1");
    fclose(tempfp);

    tempfp = fopen((prt_Path + "enable").c_str(), "w");
    fprintf(tempfp, "1");
    fclose(tempfp);

    tempfp = fopen((rudder_Path + "enable").c_str(), "w");
    fprintf(tempfp, "1");
    fclose(tempfp);
    //end initiallizing PWM and gpio pins

    //Load the engineer config file
    string lhs, eq;
    double value;
    while(configf >> lhs >> eq >> value) {
        if(eq.compare("=") != 0 || lhs.compare("#") == 0)
            continue;

        //based on example found at https://www.edureka.co/blog/maps-in-cpp/
        config_map.emplace(lhs, value);
    }
    configf.close();
    //end load engineer config file

    //Initialize libraries
    mlx90393_init(config_map["xmin"],
        config_map["xmax"],
        config_map["ymin"],
        config_map["ymax"], 0xC, 2);

    accel_init();
    if(mlx90393_get_xmax == 0) {
        ret = NOT_CALIBRATED;
    }

    pid_init(-45, 45, config_map["pg"], config_map["ig"], config_map["dg"]);

    mbt = modbus_new_rtu(CC_PATH, 9600, 'N', 8, 1);
    if(mbt == NULL) {
        cout << "MODBUS new_rtu ERROR: " << modbus_strerror(errno) << endl;
        return -1;
    }

    if(modbus_set_slave(mbt, 1) < 0) { // 1 is the degault slave address for the Renogy Rover
        cout << "MODBUS set_slave ERROR: " << modbus_strerror(errno) << endl;
    }

    if(modbus_connect(mbt) < 0) { //you probably need to be root for this to work or be part of the dialup group
        cout << "MODBUS connect ERROR: " << modbus_strerror(errno) << endl;
        return -1;
    }

    error = gps_open("localhost", "2947", &gps_Stuffs);
    (void) gps_stream(&gps_Stuffs, WATCH_ENABLE | WATCH_JSON, NULL);
    if(error != 0) {
	    cout << "GPS INIT ERROR: " << gps_errstr(errno) << endl;
        return -1;
    }

// Start engineering threads
    pthread_create(&GPS_thread, NULL, startGPSloop, NULL);
    pthread_create(&heart_beat_thread, NULL, doHeartBeat, NULL);
    pthread_create(&mag_Thread, NULL, mag_thread, NULL);

    return ret;
}

int eng_output_set_val(enum Eng_OutputDev dev, double value) {
    int outValue;

    if(dev <= 2)
    {
        if(value > 0) {
            outValue = int(value/100 * (MAX_FORWARD_DC - PWM_CENTER_DC) + PWM_CENTER_DC);
        } else {
            outValue = int(value/100 * (PWM_CENTER_DC - MIN_REVERSE_DC) + PWM_CENTER_DC);
        }
    } else if(dev == Servo_R) {
        if(value > 0) {
            outValue = int(value/90 * (MAX_RUDDER_DC - PWM_CENTER_DC) + PWM_CENTER_DC);
        } else {
            outValue = int(value/90 * (PWM_CENTER_DC - MIN_RUDDER_DC) + PWM_CENTER_DC);
        }
    }

    switch (dev)
    {
    case Motor_P:
        fseek(prt_duty_fp, 0, SEEK_SET);
        fprintf(prt_duty_fp, std::to_string(outValue).c_str());
        fflush(prt_duty_fp);
        break;
    case Motor_Both:
        fseek(prt_duty_fp, 0, SEEK_SET);
        fprintf(prt_duty_fp, std::to_string(outValue).c_str());
        fflush(prt_duty_fp);
        //fall through
    case Motor_S:
        fseek(sb_duty_fp, 0, SEEK_SET);
        fprintf(sb_duty_fp, std::to_string(outValue).c_str());
        fflush(sb_duty_fp);
        break;
    case Servo_R:
        fseek(rudder_duty_fp, 0, SEEK_SET);
        fprintf(rudder_duty_fp, std::to_string(outValue).c_str());
        fflush(rudder_duty_fp);
        break;
    case Lights:
        if(value > 0)
            pin_high(9, LIGHT_PIN);
        else
            pin_low(9, LIGHT_PIN);
        break;
    case Pump:
        if(value > 0)
            pin_high(9, PUMP_PIN);
        else
            pin_low(9, PUMP_PIN);
        break;

    default:
        return -1;
        break;
    }

    return 0;
}

void eng_panic() {
    tempfp = fopen((sb_Path + "enable").c_str(), "w");
    fprintf(tempfp, "0");
    fclose(tempfp);

    tempfp = fopen((prt_Path + "enable").c_str(), "w");
    fprintf(tempfp, "0");
    fclose(tempfp);

    tempfp = fopen((rudder_Path + "enable").c_str(), "w");
    fprintf(tempfp, "0");
    fclose(tempfp);

    modbus_close(mbt);
    modbus_free(mbt);

    mlx90393_close();

    pthread_cancel(GPS_thread);

}

power_info_t eng_get_cc_state() {
    power_info_t pinf;
    uint16_t buff[2];


    if(modbus_read_registers(mbt, BATT_VOLT_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Batt_voltage = buff[0] / 10.0;

    if(modbus_read_registers(mbt, BATT_OD_COUNT_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Batt_OverDischarge_Count = buff[0];

    if(modbus_read_registers(mbt, TEMPERATURES_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Battery_Temperature = buff[0] & 0xFF;
    pinf.Controller_Temperature = (buff[0] & 0xFF00) >> 4;

    if(modbus_read_registers(mbt, BATT_CAP_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Battery_Capacity = buff[0];

    if(modbus_read_registers(mbt, CHARGE_CRRNT_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Charging_Current = buff[0] * 0.01;

    if(modbus_read_registers(mbt, LOAD_CRRNT_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Load_Current = buff[0] * 0.01;

    if(modbus_read_registers(mbt, LOAD_PWR_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Load_Power = buff[0];

    if(modbus_read_registers(mbt, PANEL_VOLT_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Solar_Voltage = buff[0] * 0.1;

    if(modbus_read_registers(mbt, PANEL_CURRENT_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Solar_Current = buff[0] * 0.01;

    if(modbus_read_registers(mbt, AMPHR_DISCHARGED_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.AmpHrs_Discharged_today = buff[0];

    if(modbus_read_registers(mbt, AMPHR_CHARGED_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.AmpHrs_Charged_today = buff[0];

    if(modbus_read_registers(mbt, UP_DAYS_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Up_Days = buff[0];

    if(modbus_read_registers(mbt, CHARGE_STATE_ADDR, 1, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Charging_State = char(buff[0] & 0xFF);

    if(modbus_read_registers(mbt, FAULT_MSB_ADDR, 2, buff) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    pinf.Faults |= buff[0];
    pinf.Faults << 16;
    pinf.Faults |= buff[1];

    // It would probably be a good idea to do some error checking here.

    return pinf;

}


Loc_Value eng_get_latest_location(void) {
    Loc_Value output;

    pthread_mutex_lock(&gps_location_lock);
    pthread_mutex_lock(&mag_lock);

    output.accuracy = current_loc.accuracy;
    output.lat = current_loc.lat;
    output.lon = current_loc.lon;
    output.timestamp = current_loc.timestamp;
    output.mag_yaw = current_loc.mag_yaw;
    output.alt = current_loc.alt;
    output.gps_course = current_loc.gps_course;
    output.gps_velocity = current_loc.gps_velocity;
    output.flt_velocity = current_loc.flt_velocity;
    output.flt_yaw = current_loc.flt_yaw;

    pthread_mutex_unlock(&mag_lock);
    pthread_mutex_unlock(&gps_location_lock);

    return output;
}

void getGPSdata(struct gps_data_t* data){
    //gps_read(&data);
    //printf("latitude: %f, longitude: %f, speed: %f, timestamp: %lf\n", data->fix.latitude, data->fix.longitude, data->fix.speed, data->fix.time);
    pthread_mutex_lock(&gps_location_lock);
    current_loc.lat = data->fix.latitude;
    current_loc.lon = data->fix.longitude;
    current_loc.alt = data->fix.altitude;
    current_loc.accuracy = data->fix.epy; //This is just the latitude uncertainty. Maybe I'll change this to get max of lat and long
    current_loc.gps_velocity = data->fix.speed;
    current_loc.gps_course = data->fix.track;
    current_loc.timestamp = data->fix.time;
    pthread_mutex_unlock(&gps_location_lock);


    // Let's check if the boat is flooding
    if(is_high(9, WATER_SENSOR_PIN) && !error_state.water_detected) {
        error_state.water_detected = 1;
    }

    if(is_low(9, WATER_SENSOR_PIN) && error_state.water_detected)
       error_state.water_detected = 0; 

    if(accel_is_upsidedown())
        error_state.upside_down = 1;
    else
        error_state.upside_down = 0;


}

void eng_set_heading(double heading) {
    pid_set_target(heading);
}

void eng_set_mode(enum Eng_modes new_mode) {
    switch (new_mode) {
        case Autonomous:
        case Manual:
            mode = new_mode;
            break;
        case All_Stop:
            mode = Manual;
            eng_output_set_val(Motor_Both, 0);
            eng_output_set_val(Servo_R, 0);
            break;
    }
}

void eng_do_mag_cal() {
    mlx90393_begin_calibration();
}

//stop collecting data for calibration and write it to the config file
void eng_end_mag_cal() {
    pthread_mutex_lock(&mag_lock);
    mlx90393_end_calibration();
    pthread_mutex_unlock(&mag_lock);

    config_map["ymax"] = (double)mlx90393_get_ymax();
    config_map["ymin"] = (double)mlx90393_get_ymin();
    config_map["xmax"] = (double)mlx90393_get_xmax();
    config_map["xmin"] = (double)mlx90393_get_xmin();

    write_conf();
}

/**
 * This function is meant to run in its own seperate thread
 * and will get the heading from the magnetometer, update
 * current_loc, and adjust the rudder servo if in autonomous
 * mode
 */
void * mag_thread(void *) {
    double tempHeading;
    bool err = false;
    while(1) {
        
        pthread_mutex_lock(&i2c_lock);
        tempHeading = mlx90393_get_heading();
        pthread_mutex_unlock(&i2c_lock);

        if(isnan(tempHeading) && !err){
            printf("got bad heading. trying again. \n");
            err = true;
            continue;
        }

        if(mode == Autonomous) {
            double new_out = pid_get_output(tempHeading);
            eng_output_set_val(Servo_R, new_out);
            #ifdef DEBUG
            printf("did some rudderin' to %f\n", new_out);
            #endif
        }
        pthread_mutex_lock(&mag_lock);
        current_loc.mag_yaw = tempHeading;
        pthread_mutex_unlock(&mag_lock);

        err = false;
    }
}

/**
 * loop to do a low frequency heartbeat.
 * hb_on means everything is fine
 * hb_off means no APRS confirmation
 **/
void * doHeartBeat(void *) {
    while(1) {
        if(hb_on) {
            usleep(500000); 
            pin_high(9, HBT_PIN);
            usleep(500000);
            pin_low(9, HBT_PIN);
        } else {
            sleep(1);
            pin_high(9, HBT_PIN);
            sleep(1);
            pin_low(9, HBT_PIN);
        }
    }
}

/**
 * baed on code from https://thispointer.com/how-to-iterate-over-a-map-in-c/
 * */
void write_conf() {
    //configf.beg();
    configf.open(CONFIG_PATH);
    printf("%d %d \n", configf.bad(), configf.fail());
    for (std::pair<std::string, double> element : config_map) {
        configf << element.first << " = " << element.second << std::endl;
    }
    configf.close();
}
