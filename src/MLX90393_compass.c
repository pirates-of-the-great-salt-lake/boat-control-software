#include <iobb.h>
#include <stdio.h>
#include <math.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <gpiod.h>
#include <errno.h>

#define SB 0x10 //zyxt start burst mode
#define SW 0x20 //zyxt start WOC mode
#define SM 0x30 //zyxt start single measurement mode
#define RM 0x40 //zyxt read measurement
#define RR 0x50 //0abc  byte 2:{A5…A0,0,0} read register
#define WR 0x60 //0abc D15…D8 D7…D0 {A5…A0,0,0} write register
#define EX 0x80 // Exit mode
#define HR 0xD0 // Memory Recall
#define HS 0xE0 // Memory Store
#define RT 0xF0 // Reset

#define DECLINATION 12.5
#define ON_DEBIAN

// Function prototypes
unsigned short getRegister(unsigned char reg);
void setRegister(unsigned char reg_addr, unsigned char value, unsigned short mask, unsigned char shift);
void set_HALL_CONF(unsigned char value);
void set_GAINSEL(unsigned char value);
void set_BIST(unsigned char value);
void set_BURST_DATA_RATE(unsigned char value);
void set_TCMP_EN(unsigned char value);
void set_DIG_FILT(unsigned char value);
void set_RESX(unsigned char value);
void set_RESY(unsigned char value);
void set_RESZ(unsigned char value);
void set_OSR(unsigned char value);
void set_BURST_SEL(unsigned char value);

static short xmin, xmax, ymin, ymax;
static double offsetx, offsety, scalex, scaley;
int i2c_handle;
static bool is_calibrating;
unsigned static char address;

#ifndef	CONSUMER
#define	CONSUMER	"Consumer"
#endif

struct gpiod_line_event event;
struct gpiod_chip *chip;
struct gpiod_line *line;

struct timespec ts = { 10, 0 };
//ts.tv_sec = 10;

//This will put the magnetometer into burst mode
int mlx90393_init(short x_min, short x_max, short y_min, short y_max, unsigned char addr, int i2c_bus) {
    is_calibrating = false;
    xmin = x_min;
    xmax = x_max;
    ymin = y_min;
    ymax = y_max;

    address = addr;

    iolib_init();
    #ifdef DEBIAN
    configure_i2c_pins(19,20);
    #endif

    i2c_handle = i2c_open(i2c_bus, addr);

    unsigned char status;
    i2c_write_byte(i2c_handle, EX); //Get out of burst mode
    i2c_read_byte(i2c_handle, &status);
    fflush(stdout);
    printf("starting mag init. status: %#04X \n", status);
    iolib_delay_ms(100); // wait for the magnetometer to reset

    set_GAINSEL(7);
    set_RESX(0);
    set_RESY(0);
    set_BIST(0);
    set_HALL_CONF(0x0C);
    set_TCMP_EN(0);
    set_BURST_DATA_RATE(1);
    set_DIG_FILT(7);
    set_OSR(3);
    set_BURST_SEL(0);

    i2c_write_byte(i2c_handle, SB | 0x6); //Enter Burst Mode!!
    i2c_read_byte(i2c_handle, &status);

    i2c_write_byte(i2c_handle, SB | 0x6); //Enter Burst Mode!!
    i2c_read_byte(i2c_handle, &status);

    chip = gpiod_chip_open_by_name("gpiochip1");
	if (!chip) {
		perror("Open chip failed\n");
        return -1;
	}

    //probably should do some error checking here
    line = gpiod_chip_get_line(chip, 19); //p9_16 the corresponding pin on the BB can be found by running gpioinfo
    if(line == nullptr)
	    printf("Error requesting line");
    int err = gpiod_line_request_rising_edge_events(line, CONSUMER);
    if(err)
	    printf("Error requesting rising edge events");

    double avg_dx, avg_dy, avg_d;

    offsetx = (xmax + xmin) / 2;
    offsety = (ymax + ymin) / 2;

    avg_dy = (ymax - ymin) / 2;
    avg_dx = (xmax - xmin) / 2;
    avg_d = (avg_dx + avg_dy) / 2;

    scalex = avg_d / avg_dx;
    scaley = avg_d / avg_dy;

}

void mlx90393_close() {
    unsigned char status;
    i2c_write_byte(i2c_handle, EX); //Get out of burst mode
    i2c_read_byte(i2c_handle, &status);
    i2c_close(i2c_handle);
}

/** Blocking function to retrieve heading while in burst mode
 * This function returns a heading in degrees clockwise from North
 * between 0 and 360
 */
double mlx90393_get_heading() {
    static bool err = false;
    unsigned char write_bytes[4];
    unsigned char read_buf[7];

    int ret = gpiod_line_event_wait(line, &ts); //Block for the trigger pin
    if(ret <= 0 && !err) {
	    printf("interrupt wait error\n");
	    printf("%d \n", errno);
        err = true;
        return nan("");
    }
    else if (ret == 0 && !err) {
	    printf("interrupt timeout\n");
        err = true;
        return nan("");
    }

    ret = gpiod_line_event_read(line, &event);
    if(ret < 0 && !err) {
	   printf("line event read errno: %d \n", errno); 
       err = true;
       return nan("");
    }
    //ret will be less than 0 if there are any errors
    write_bytes[0] = RM | 0x06; // read x and y measurements

    if(i2c_write_read(i2c_handle, address, write_bytes, 1, address, read_buf, 5) < 0 && !err) {
        printf("i2c log error\n");
        err = true;
        return nan("");
    }

    err = false;
    short t=0;
    short y=0;
    short x=0;

    x |= ((unsigned short)read_buf[1]) << 8;
    x |= read_buf[2];

    y |= ((unsigned short)read_buf[3]) << 8;
    y |= read_buf[4];

    double avg_dx, avg_dy, avg_d;
    double correctedx, correctedy;

    if(is_calibrating) {
        if(x > xmax)
            xmax = x;

        if(x < xmin)
            xmin = x;

        if(y > ymax)
            ymax = y;

        if(y < ymin)
            ymin = y;
    }         
    offsetx = (xmax + xmin) / 2;
    offsety = (ymax + ymin) / 2;

    avg_dy = (ymax - ymin) / 2;
    avg_dx = (xmax - xmin) / 2;
    avg_d = (avg_dx + avg_dy) / 2;

    if(avg_d == 0 || avg_dx == 0)
	scalex = 1;
    else
    	scalex = avg_d / avg_dx;

    if(avg_d == 0 || avg_dx == 0)
	scaley = 1;
    else
    	scaley = avg_d / avg_dy;


    correctedx = (x - offsetx) * scalex;
    correctedy = (y - offsety) * scaley;
    double heading = (180.0 * atan(correctedy/correctedx)) / 3.14159;

    if(heading < 0) {
        if(correctedy > 0 )
            heading += 180;
        else
            heading += 360;
    }
    else {
        if(correctedx < 0)
            heading += 180;
    }

    heading += DECLINATION;

    if(heading > 360)
        heading -= 360;

    return heading;
}

void mlx90393_begin_calibration() {
    is_calibrating = true;
}

void mlx90393_end_calibration() {
    is_calibrating = false;
}

short mlx90393_get_xmin() {
    return xmin;
}

short mlx90393_get_ymin() {
    return ymin;
}

short mlx90393_get_xmax() {
    return xmax;
}

short mlx90393_get_ymax() {
    return ymax;
}


unsigned short getRegister(unsigned char reg) {
    unsigned char read_buf[7];
    unsigned short output = 0;
    unsigned char readStuffs[2];
    readStuffs[0] = RR;
    readStuffs[1] = reg << 2;
    i2c_write(i2c_handle, readStuffs, 2);
    i2c_read(i2c_handle,read_buf,3);
    output |= read_buf[1];
    output <<= 8;
    output |= read_buf[2];
    return output;
}

void setRegister(unsigned char reg_addr, unsigned char value, unsigned short mask, unsigned char shift) {
    unsigned char read_buf[7];
    unsigned char write_bytes[4];
    unsigned short reg0 = getRegister(reg_addr);
    reg0 &= mask; //Set BDR to all zeros
    reg0 |= ((unsigned short)value) << shift;
    write_bytes[0] = WR;
    write_bytes[3] = reg_addr << 2;
    write_bytes[1] = (reg0 >> 8);
    write_bytes[2] = (reg0 & 0x00FF);

    i2c_write(i2c_handle, write_bytes,4);
    i2c_read(i2c_handle,read_buf,1);

}

void set_HALL_CONF(unsigned char value) {
    setRegister(0, value, 0xFFF0,0);
}

void set_GAINSEL(unsigned char value) {
    setRegister(0, value, 0xFF8F, 4);
}

void set_BIST(unsigned char value) {
    setRegister(0, value, 0xFEFF, 8);
}

void set_BURST_DATA_RATE(unsigned char value) {
    setRegister(1, value, 0xFFC0, 0);
}

void set_TCMP_EN(unsigned char value) {
    setRegister(1, value, 0xFBFF, 10);
}

void set_DIG_FILT(unsigned char value) {
    setRegister(2, value, 0xFFE3, 2);
}

void set_RESX(unsigned char value) {
    setRegister(2, value, 0xFF9F, 5);
}

void set_RESY(unsigned char value) {
    setRegister(2, value, 0xFE7F, 7);
}

void set_RESZ(unsigned char value) {
    setRegister(2, value, 0xF9FF, 9);
}

void set_OSR(unsigned char value) {
    setRegister(2, value, 0xFFFC, 0);
}

void set_BURST_SEL(unsigned char value) {
    setRegister(1, value, 0xFC3F, 6);
}
