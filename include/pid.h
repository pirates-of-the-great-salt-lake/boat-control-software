/**
 * A pid will be implemented here
 */


int pid_init(double min, double max, double p_gain, double i_gain, double d_gain);

void pid_err_reset();

void pid_set_target(double target);

double pid_get_output(double current_value);