#ifndef CONFIG
#define CONFIG value

/**
Called upon startup, loads config data from settings.config.
*/
void config_init(void);

void config_setI(int module, int setting, int value);

void config_setD(int module, int setting, double value);

void config_setSTR(int module, int setting, char* value);

int config_getI(int module, int setting);

double config_getD(int module, int setting);

char* config_getSTR(int module, int setting);



#endif
