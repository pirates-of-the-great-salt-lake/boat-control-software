#ifndef DISPATCH_H
#define DISPATCH_H value

typedef int EVENT_HANDLE_t;

/**
Initializes the dispatch module.
*/
void dispatch_init(void);

/**
Creates a new event and returns a corresponding event handle. The event_arg_size
specifies the size of the argument taken by the event handlers.
*/
EVENT_HANDLE_t dispatch_create_event(size_t event_arg_size);

/**
Triggers an event and fires all registered handlers on the current thread.
The passed event_arg may be modified by the event handlers. Each event handler
will also run on the current thread. The method will return once all event
handlers have been run.
*/
void dispatch_trigger(EVENT_HANDLE_t event, void* event_arg);

/**
Identical to dispatch_trigger, with TWO CRUCIAL DIFFERENCES! The event_arg
WILL NOT be modified by any handler, and the handlers will all be run on
a single seperate thread. This method will return immediately. The event_arg
MUST BE SHALLOW! The behavior is undefined for deep structs or objects.
*/
void dispatch_trigger_async(EVENT_HANDLE_t event, void* event_arg);

/**
Registers the given event handler as a listener of the specified event.
*/
void dispatch_register_listener(EVENT_HANDLE_t event, void (*event_handler)(void*) );

/**
Deregisters the given event handler of the specified event.
*/
void dispatch_deregister_listener(EVENT_HANDLE_t event, void (*event_handler)(void*));

#endif
