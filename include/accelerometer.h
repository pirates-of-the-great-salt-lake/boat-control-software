#ifndef ACCELEROMETER
#define ACCELEROMETER

int accel_init();

bool accel_is_upsidedown();

#endif