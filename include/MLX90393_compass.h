#ifndef mlx90393
#define mlx90393


/**
 * This header contains the methods to use the mlx90393 magnetometer as a compass
 */

int mlx90393_init(short x_min, short x_max, short y_min, short y_max, unsigned char addr, int i2c_bus);
void mlx90393_close();

/** Blocking function to retrieve heading while in burst mode
 * This function returns a heading in degrees clockwise from North
 * between 0 and 360
 */

double mlx90393_get_heading();

void mlx90393_begin_calibration();
void mlx90393_end_calibration();

short mlx90393_get_xmin();
short mlx90393_get_ymin();
short mlx90393_get_xmax();
short mlx90393_get_ymax();


#endif