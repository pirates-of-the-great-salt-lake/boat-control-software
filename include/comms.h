#ifndef COMMS
#define COMMS

#include <string>
#include "engineer.h"
#include "navigator.h"
#include "aprs.h"

extern "C" int run_server();
extern "C" void read_and_send(int sock_fd);


enum Commands{
    // captain specific command
    CMD_Capt_Control_Mode = 1,
    
    // engineer specific commands
    CMD_Eng_Mode = 2,
    CMD_Motor_Both = 3,
    CMD_Motor_S = 4,
    CMD_Motor_P = 5,
    CMD_Servo_R = 6,
    CMD_Pump = 7,
    CMD_Lights = 8,
    CMD_Heading = 9,
    CMD_Eng_Panic = 10,

    // nav specific commands
    CMD_Nav_Mode = 11,
    CMD_Add_Waypoint = 12,
    CMD_Insert_Waypoint = 13,

    // get engineer status commands
    CMD_Course,
    CMD_Get_Lat,
    CMD_Get_Lon,
    CMD_Get_Alt,
    CMD_Get_Velocity,
    CMD_Get_Heading,
    CMD_Get_Batt_Votlage,
    CMD_Get_Batt_OverDischarge_Count,
    CMD_Get_Battery_Temperature,
    CMD_Get_Battery_Capacity,
    CMD_Get_Charging_Current,
    CMD_Get_Controller_Temperature,
    CMD_Get_Load_Current,
    CMD_Get_Load_Power,
    CMD_Get_Solar_Voltage,
    CMD_Get_Solar_Current,
    CMD_Get_AmpHrs_Discharged_today,
    CMD_Get_AmpHrs_Charged_today,
    CMD_Get_Up_Days,
    CMD_Get_Charging_State,
    CMD_Get_Power_Faults,

    // comm specific commands
    CMD_Log_Mode,
    CMD_Report_Frequency,
    CMD_Broadcast_Status_Now 
};

struct Command {
  enum Commands command_code;
  int i1_value;
  int i2_value;
  double d1_value;
  double d2_value;
};


#include "captain.h"
/*  Creates a log of the provided message.  Logs are contained at ../logs/ and
 * will have a filename in the format MMM_DD_YYYY.log 
 * Only one process can log at a time - this is a blocking call but should not take long. */
void create_log(const std::string &message);

// Used to create a log as well as sending the contents as a message. Note that messages
// are sent out in 67 character segments if longer than 67 characters.  The characters '~', 
// '|', and '{' are reserved and will be removed from the message if found before sending.
// The send portion of this is asynchronous and will return once the message is logged.
void log_and_send(const std::string &message);

// Changes the report frequency to the provided number of seconds.
void change_report_frequency(int seconds);

// start up comm events
void comm_init();

// comm entry point
void comm_startup();

// Continuously updates status for webserver as well as for auto-reports
void auto_update_status();

// helper function to remove APRS reserved characters
std::string remove_reserved_chars(const std::string &s);

// Runs in an infinite loop in its own thread.  Periodically checks if there are messages 
// to parse for commands (both from APRS or in TCP/IP buffer) or messages from other components to send.  
void message_server();

// endless loop to make start server and restart if necessary
void start_tcpip_server();

// Listens to events from other boat components. Creates a log from provided structure.
void com_register_command_listener(void*(char* command));

// Sends the provided message.  Returns true if the message was acknowledged in 8 attempts
// or less. (using APRS decaying resend pattern, starting at 5 seconds and capping at 10 minutes)
bool send_message(std::string message);

// copies the tcpip buffer to comms module to process commands
void copy_rcvd_data(char *data, int num_bytes_to_copy);

// Function allows the server to request all messages currently in the message queue to send
int get_messages_to_send(char *data, int buffer_size);

// processes commands in either a APRS or tcpip received buffer
void process_command_buffer(char *buffer, int num_bytes);

// sets the tcpip mode by the server
void enable_tcpip_mode(int using_tcpip);


void send_status_report();
#endif
