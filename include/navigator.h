
#ifndef NAVIGATOR
#define NAVIGATOR value

enum Nav_Mode {
  //Navigator takes no action
  Off = 0,
  Waypoint_Path = 1,
  Waypoint_Target = 2
};

enum Nav_FOption {
  //Option for maximum speed,
  Max_Speed = 1,
  //Option for maximum power usage
  Max_Power = 2
};

enum Nav_IOption {
  //Collision override level
  Collision_Override = 3
};
/**
A status enum used to describe the state of the nav points.
Active: Valid navpoint that will be navigated to some point in the future, but
  is not currently being navigated towards.
Target: The navpoint that is currently the target of the navigator.
Completed: A navpoint that has already been successfully navigated towards.
Abandoned: A navpoint that has been abandoned by the navigator, probably because
  the boat was closer to an active waypoint than the current waypoint (or some
  other policy abandoned the navpoint).
*/
enum Nav_Point_Status{
  Active = 1,
  Target = 2,
  Completed = 3,
  Abandoned = 4
};

/**
A simple 2d point.
*/
struct point {
  double x, y;
};

/**
Linked list member of a navpoint list, which forms a path.
*/
typedef struct nav_point {
  struct point location;
  struct nav_point* next;
  struct nav_point* prev;
  enum Nav_Point_Status status;
  double radius;
  int indx;
} Nav_Point;

enum NavPoint_Event {
	PathCompleted = 1,
	PathAbandoned = 2,
	PathActive = 3,
};

/**
Called at runtime during setup. Configures any/all events.
*/
void nav_init(void);

static void* nav_thread(void*);

/**
Starts the navigator module, this is non-blocking, creates necessary threads and begins
normal execution of the engineer module. Does not return until engineer module is fully
configured and ready for execution.
*/
void nav_startup(int wait_for_completed=0);

/**
Convert value from degrees to radians
*/
double toRad (double);
/**
Find the distance between two points with a latitudee and a longtitude 
*/
double distance(point, point);

/**
Find the angle in radian between two points with a latitudee and a longtitude 
*/
double bearing(point, point);

/**
Called if module must be reloaded---
Useful for tests!
*/
void nav_deinit(void);

/**
Set the operating mode of the navigation module.
*/
void nav_set_mode(enum Nav_Mode);

/**
Get the operating mode of the navigator module.
*/
enum Nav_Mode nav_get_mode(void);
/**
set the specified float option
*/
void nav_set_optionf(enum Nav_FOption, float);

/**
set the specified integer option
*/
void nav_set_optioni(enum Nav_IOption, int);

/**
Stop the navigation module, but do so after setting related modules to a safe state.
*/
void nav_shutdown(void);

/**
Add points from the provided list of nav points.
Each new point will be added to the end of the list, and the
status of each point will be updated to match its position in the list.
*/
void nav_path_add_path(Nav_Point*);

/**
Insert a navpoint list into the active navpoints at the specified location.
The index of the insertion will be relative to the current navpoint (index 0).
The navpoint at the specified index will be shifted to the end of the inserted
path segment.
*/
void nav_path_insert_path(int, Nav_Point*);

/**
Gets the current nav point path list, with the returned navpoint being
the target navpoint.
*/
Nav_Point* nav_get_path(void);

/**
Gets a list of pointers to all navpoints that have been abandoned since the last
time clear_abandoned_navpoints() was called.
*/
Nav_Point** nav_get_abandoned_navpoints(void);

/**
Deallocates all abandoned navpoints.
*/
void nav_clear_abandoned_navpoints(void);

/**
Deallocates completed navpoints.
*/
void nav_clear_completed_navpoints(void);

/**
Clears and deallocates all navpoints, and forces the navigator to the Off mode.
*/
void nav_clear_all_navpoints(void);

/**
Save the state of the navigator to file.
*/
void nav_savestate(void);

/**
Load the state of the navigator from file.
*/
void nav_loadstate(void);

/**
Helper function to load each nav point from file.
*/
Nav_Point* nav_loadnext(nav_point*, FILE*);

/**
Print the list of the navigator showing the location/status of each point starting at the head.
*/
void nav_print_path();

#endif
