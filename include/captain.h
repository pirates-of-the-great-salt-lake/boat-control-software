#ifndef CAPTAIN
#define CAPTAIN

#include "comms.h"

enum Control_Mode{
  cap_Off = 0,
  cap_Manual = 1,
  cap_Autonomous_Path = 2,
  cap_Autonomous_Hold = 3
};

void cap_get_control_mode(void);

bool cap_execute_command(Command command);

#endif
