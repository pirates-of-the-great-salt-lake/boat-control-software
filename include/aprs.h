#ifndef APRS
#define APRS aprs


// This fills in the provided data buffer with all received APRS messages up to the provided buffer size and returns
// the number of bytes that were filled in. If there are no messages to fill the buffer with returns -1.
int get_buffer(char *data, int buffer_size);

/*
  Course
  Timestamp
  Pos
  CustomData
*/

typedef struct APRS_Rep {
  float course;
  double lat,lon;
  double timestamp;
  char * customData;
} APRS_Report;

void aprs_init();

/**

*/
int aprs_send(char *message);

#endif
