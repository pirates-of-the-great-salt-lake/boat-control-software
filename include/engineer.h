#ifndef ENGINEER
#define ENGINEER
#define NOT_CALIBRATED 2

enum Eng_modes {
  All_Stop = 0,
  Manual = 1,
  Autonomous = 2
};

enum Eng_OutputDev {
  Motor_Both = 0,
  Motor_S = 1,
  Motor_P = 2,
  Servo_R = 3,
  Pump = 4,
  Lights = 5
};

enum Eng_InputDev {
  NineDOF = 1,
  GPS = 2,
  HullMoisture = 3,
  ChargeControl = 4
};

typedef struct {
  /**
  Location parameters, latitude, longitude, altitude.
  */
  double lat, lon, alt;
  /**
  Radius of possible true coordinates.
  */
  double accuracy;
  /**
  Velocity and velocity course according to gps unit.
  */
  double gps_velocity, gps_course;
  /**
  Compass heading corrrected for true north by 12.5 degrees.
  */
  double mag_yaw;
  /**
  Filtered velocity and filtered yaw.
  */
  double flt_velocity, flt_yaw;
  /**
  Timestamp of the OLDEST information in this location struct.
  */
  double timestamp;
} Loc_Value;

/**
 * This struct holds all information that can be retrieved from 
 * the Renogy charge controller over ModBus.
 */
typedef struct {
  float Batt_voltage;
  short Batt_OverDischarge_Count;
  short Battery_Temperature;
  short Battery_Capacity;
  float Charging_Current;
  short Controller_Temperature;
  float Load_Current;
  short Load_Power;
  float Solar_Voltage;
  float Solar_Current;
  short AmpHrs_Discharged_today;
  short AmpHrs_Charged_today;
  short Up_Days;


  /* Charging State Bit description.
  copied from Rover 20A/40A Charge Controller—MODBUS Protocol
  00H: charging deactivated
  01H: charging activated
  02H: mppt charging mode
  03H: equalizing charging mode
  04H: boost charging mode
  05H: floating charging mode
  06H: current limiting (overpower)
  */
  char Charging_State;

  /*  Fault bit descriptions
    copied from Rover 20A/40A Charge Controller—MODBUS Protocol
    b31 reserved
    b30: circuit, charge MOS short circuit
    b29: anti-reverse MOS short
    B28: solar panel reversely connected
    B27: solar panel working point over-voltage
    B26: solar panel counter-current
    B25: photovoltaic input side over-voltage
    B24: photovoltaic input side short circuit
    B23: photovoltaic input overpower
    B22: ambient temperature too high
    B21: controller temperature too high
    B20: load overpoweror load over-current
    B19: load short circuit
    B18: battery under-voltage warning
    B17: battery over-voltage
    B16: battery over-discharge
    B0-B15 reserved
 */
  unsigned int Faults;


} power_info_t;

/**
 * This struct contains 9 bits where each bit represents a different error state;
 * The engineer will set each one of these to true if that error occurs.
 */
typedef struct{
  //todo: depends on what error codes can be gotten from the system
  bool battery_failure:1;
  bool solar_power_lost:1;
  bool water_detected:1;
  bool gps_failure:1;
  bool cc_comms_lost:1;
  bool compass_failure:1;
  bool upside_down:1;
  bool battery_over_temp:1;
  bool general_over_temp:1;

}Error;

struct Loc_Update {
  Loc_Value raw_gps;
};

/**
Initializes engineer module, creates relavent events
*/
void eng_init();

/**
Starts the engineer module, this is blocking, creates necessary threads and begins
normal execution of the engineer module. Does not return until engineer module is fully
configured and ready for execution.
*/
int eng_startup(enum Eng_modes mode);

/**
 * changes the mode between autonomous or manual or All Stop
 */
void eng_set_mode(enum Eng_modes);

/**
Sets the target heading in degrees
*/
void eng_set_heading(double heading);

/**
 * set the megnetometer to do calibration. The magnetometer will need to be rotated in a 
 * circle to find the max and min for each axis. Calling this function will not change the 
 * way the rest of the engineer operates
 * */
void eng_do_mag_cal();

/**
 * Stops grabing the max and min for each axis on the magnetometer and saves the calibration values
 * to engineer.conf
 * */
void eng_end_mag_cal();

/**
 * Get the status of the Renogy charge controller
 */
power_info_t eng_get_cc_state();

/**
Sets the output device to the prescribed value. Returns nonzero if the value
could not be set. Call eng_get_last_error(outputdev) to see why. For setting
drive motors, value should be between -100 (full reverse power) and 100 (full
forward power). for controlling the steering servo, the value should be
between -90 and +90 where 0 is the center point.
*/
int eng_output_set_val(enum Eng_OutputDev, double value);

/**
Gets the last error for the particular output device.
*/
extern "C" Error eng_get_last_error(enum Eng_OutputDev);

/**
Gets the most recent location information.
*/
Loc_Value eng_get_latest_location(void);

/**
Force the engineer to shut down all peripheral systems in a safe manner.
*/
void eng_panic(void);


#endif
