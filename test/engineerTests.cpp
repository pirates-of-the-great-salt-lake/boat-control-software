#include "engineer.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <curses.h>

using namespace std;

int initializeTest();
int testOutputs();
int testAutonomous();
int testCC_Coms();
void test_lights();
void test_pump();
void test_compassnav();
void testCalibration();
int power = 0;
int rudder = 0;
int ton = 0;

int main(int argc, char** argv) {
    if(argc >= 2)
	power = stoi(argv[1]);
    if(argc >= 3)
	rudder = stoi(argv[2]);
    if(argc >= 4)
	ton = stoi(argv[3]);

    initializeTest();
    eng_output_set_val(Lights, 0);
    //testCalibration();
    //testOutputs();
   // sleep(2);
    test_compassnav();
    
    //testAutonomous();
    //test_lights();
    //test_pump();
    //testCC_Coms();

    return 0;
}

int initializeTest() {
    eng_startup(Manual);
    sleep(2);
    cout << "dun init\n";
    return 0;
}

int testOutputs() {
    initscr();
    
    while(1){
      char ch = getch();
      if(ch == 'a')
	      rudder += 5;
      if(ch == 'd')
	      rudder -= 5;
      if(ch == 'w'){
      eng_output_set_val(Motor_S, power + rudder);
      eng_output_set_val(Motor_P, power - rudder);
      }else
      if(ch == 's'){
      eng_output_set_val(Motor_S, -power - rudder);
      eng_output_set_val(Motor_P, -power + rudder);
      }else{

        eng_output_set_val(Motor_S, 0);
        eng_output_set_val(Motor_P, 0);
      }
      //eng_output_set_val(Servo_R, rudder);
      printw("rudder: %d, speed: %f", rudder, eng_get_latest_location().gps_velocity);
    }
    eng_output_set_val(Motor_S, 0);
    eng_output_set_val(Motor_P, 0);
    eng_output_set_val(Servo_R, 0);
/*
    cout << "testing port at -%50 for 3 sec\n";
    eng_output_set_val(Motor_P, -50);
    sleep(2);

    cout << "testing both at %50 for 3 sec\n";
    eng_output_set_val(Motor_Both, 50);
    sleep(2);
    cout << "shutoff\n";
    eng_output_set_val(Motor_Both, 0);
*/
    //cout << "test rudder servo in 5 degree increments\n";
    //for(int i = -90; i <= 90; i+=5) {
   //     eng_output_set_val(Servo_R, i);
    //    sleep(1);
   // }

    return 0;

}

void test_pump(void){
    for(int i = 0; i < 10; i++){
        eng_output_set_val(Pump, 1);
	sleep(2);
        eng_output_set_val(Pump, 0);
	sleep(2);
    }

}

void test_lights(void){
    for(int i = 0; i < 10; i++){
        eng_output_set_val(Lights, 1);
	sleep(2);
        eng_output_set_val(Lights, 0);
	sleep(2);
    }

}
int testCC_Coms(){
    power_info_t Pinf = eng_get_cc_state();
    cout << "Batt_voltage: " << Pinf.Batt_voltage << "\n";
    cout << "Batt_OverDischarge_Count: " << Pinf.Batt_OverDischarge_Count << "\n";
    cout << "Battery_Temperature: " << Pinf.Battery_Temperature << "\n";
    cout << "Battery_Capacity: " << Pinf.Battery_Capacity << "\n";
    cout << "Charging_Current: " << Pinf.Charging_Current << "\n";
    cout << "Controller_Temperature: " << Pinf.Controller_Temperature << "\n";
    cout << "Load_Current: " << Pinf.Load_Current << "\n";
    cout << "Load_Power: " << Pinf.Load_Power << "\n";
    cout << "Solar_Voltage: " << Pinf.Solar_Voltage << "\n";
    cout << "Solar_Current: " << Pinf.Solar_Current << "\n";
    cout << "AmpHrs_Discharged_today: " << Pinf.AmpHrs_Discharged_today << "\n";
    cout << "AmpHrs_Charged_today: " << Pinf.AmpHrs_Charged_today << "\n";
    cout << "Up_Days: " << Pinf.Up_Days << "\n";
  

    cout << "Charging State Bit description.\n";
    cout << "copied from Rover 20A/40A Charge Controller—MODBUS Protocol\n";
    cout << "00H: charging deactivated\n";
    cout << "01H: charging activated\n";
    cout << "02H: mppt charging mode\n";
    cout << "03H: equalizing charging mode\n";
    cout << "04H: boost charging mode\n";
    cout << "05H: floating charging mode\n";
    cout << "06H: current limiting (overpower)\n";
    cout << "\n";
    cout << "Charging_State: " << hex << Pinf.Charging_State << endl;

    cout << "Fault bit descriptions" << endl;
    cout << "copied from Rover 20A/40A Charge Controller—MODBUS Protocol" << endl;
    cout << "b31 reserved" << endl;
    cout << "b30: circuit, charge MOS short circuit " << endl;
    cout << "b29: anti-reverse MOS short " << endl;
    cout << "B28: solar panel reversely connected" << endl;
    cout << "B27: solar panel working point over-voltage" << endl;
    cout << "B26: solar panel counter-current" << endl;
    cout << "B25: photovoltaic input side over-voltage" << endl;
    cout << "B24: photovoltaic input side short circuit " << endl;
    cout << "B23: photovoltaic input overpower" << endl;
    cout << "B22: ambient temperature too high" << endl;
    cout << "B21: controller temperature too high" << endl;
    cout << "B20: load overpoweror load over-current" << endl;
    cout << "B19: load short circuit" << endl;
    cout << "B18: battery under-voltage warning" << endl;
    cout << "B17: battery over-voltage" << endl;
    cout << "B16: battery over-discharge" << endl;
    cout << "B0-B15 reserved" << endl;
    cout << endl;
 
    cout << Pinf.Faults; 
    return 0;
}

void test_compassnav() {
    

    for(int i = 0; i < 3; i++) {
        Loc_Value location = eng_get_latest_location();
    	cout << "mag heading: " << location.mag_yaw << endl;
        cout << "lat: " << location.lat << endl;
        cout << "lon: " << location.lon << endl;
        cout << "speed: " << location.gps_velocity << endl;
        cout << "altitude: " << location.alt << endl;
	sleep(2);
    }

}


int testAutonomous() {
    double avgHeading;

    for(int i = 0; i < 10; i++) {
        usleep(100000);
        avgHeading += eng_get_latest_location().mag_yaw;
    }

    avgHeading /= 10;

    eng_set_heading(avgHeading);
    cout << "got average heading of: " << avgHeading << " entering autonomous mode" << endl;
    eng_set_mode(Autonomous);
    cout << "\n\n\n CALIBRATING \n\n\n";
    eng_do_mag_cal();
    sleep(60);
    eng_end_mag_cal();
    cout << "\n\n\n END OF CALIBRATION \n\n\n";
    sleep(240);
}

void testCalibration() {
    cout << "starting calibration" << endl;
    eng_do_mag_cal();
    sleep(30);
    cout << "ending calibration" << endl;
    eng_end_mag_cal();
}
