#include "accelerometer.h"
#include <iostream>
#include <unistd.h>
using namespace std;


int main() {
    accel_init();

    while(1) {
        sleep(1);
        if(accel_is_upsidedown()) {
            cout << "I'm upside down!!!" << endl;
        } else {
            cout << "NOT upside down!!!!" << endl;
        }
    }
}