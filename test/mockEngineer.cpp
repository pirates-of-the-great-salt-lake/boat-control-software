/**
 * TODO: setup threads
 * TODO: do error checking
 * TODO: interface with comms
 * TODO: setup appropriate events
 * 
 * */

#define DEBIAN

#ifdef DEBIAN
#define SB_PATH "/sys/class/pwm/pwm-1:0/"
#define PRT_PATH "/sys/class/pwm/pwm-1:1/"
#define RUDDER_PATH "/sys/class/pwm/pwm-4:0/"
#else
#define SB_PATH "/sys/class/pwm/pwmchip0/pwm0/"
#define PRT_PATH "/sys/class/pwm/pwmchip0/pwm1/"
#define RUDDER_PATH "/sys/class/pwm/pwmchip2/pwm0/"
#endif

/**
 * rudder pwm1a p14
 * hbt 12
 * pto 11
 * 13 nav light
 * 15 pump cutoff (pump off high)
 * 16 mag trigger
 * 17 water_sense (active high)
 * 19 scl
 * 20 sda
 * 21 pwm0b port motor
 * 22 pwm0a strbrd
 * 24 tx cc
 * 36 rcv
 **/
#define CC_PATH "/dev/ttyUSB0"
#define MAX_FORWARD_DC 1700000
#define MIN_REVERSE_DC 1300000
#define MAX_RUDDER_DC 2600000
#define MIN_RUDDER_DC 450000
#define PWM_CENTER_DC 1500000

#define LIGHT_PIN 15 //all of these are on pin header 9
#define PUMP_PIN 16

#include "engineer.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

Loc_Value values [3] = { {41.246079, -112.288213, 1282, 0, 0, 0, 0, 50, 0, 0}, {41.233308, -112.331844, 1282, 0, 0, 0, 0, 270, 0, 0}, {41.248798, -112.372040, 1282, 0, 0, 0, 0, 277, 0, 0} };
int counter = 0;
using namespace std;
string sb_Path, prt_Path, rudder_Path;
bool is_initialized = false;
FILE *tempfp, *sb_duty_fp, *prt_duty_fp, *rudder_duty_fp ;
unsigned char i2c_buffer[7];
int i2c_handle;
pthread_t GPS_thread, mag_Thread, daq_thread; //short for data acqusition thread
pthread_mutex_t gps_location_lock, mag_lock;

Loc_Value current_loc;

enum last_location{compass, gps};
int last_sensor;

void getGPSdata(struct gps_data_t* data);
void getMagHeading();

int eng_startup(enum Eng_modes) {
    return 0;
}

int eng_output_set_val(enum Eng_OutputDev dev, double value) {

    return 0;
}

void eng_panic() {

}

power_info_t get_cc_state() {
    power_info_t pinf;

    // It would probably be a good idea to do some error checking here.

    return pinf;

}


Loc_Value eng_get_latest_location(void) {
    Loc_Value temp = values[counter];

    if (counter == 3)
    {
        counter = 0;
    }
    else
    {
        counter++;
    }
    return temp;
}

void getGPSdata(struct gps_data_t* data){
}

/**
 * This function is meant to run in its own seperate thread
 * and will get the heading from the magnetometer, update
 * current_loc, and adjust the rudder servo if in autonomous
 * mode
 */
void getMagHeading() {
}

void eng_set_heading(double heading)
{
    return;
}
