#ifndef TEST_DISPATCH_H
#define TEST_DISPATCH_H value

int init_test_dispatch(void);

int clean_test_dispatch(void);

int register_test_dispatch(void);

#endif
