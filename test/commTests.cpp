#include <unistd.h>
#include <iostream>
#include "comms.h"
#include <string.h>


  
const static double latitudes[] = {-14313.45, -14313, -1113.35, -5313, -813.4543, -813, -42.01, -32, -4.342, -6, -.243, 
                                   .01, 8.0, 8.435, 17, 34.3242, 123, 147.3242, 1234.234, 3948, 14234.98, 17049};
const static double longitudes[] = {8014.234, 4314, 414.234, 614, 14.34, 29, 4.345, 2, .24
                                    -.98, -4.324, -14, -42.345, -325.345 -543, -6023.234, -6545};


int main()
{
  comm_startup();

  create_log("This is a test log.");

  log_and_send("~{|This is a test to make sure that ||||illegal ~~~characters are removed{{{{ and that re|~{lly long messages are properly broken up~~ into multiple transmissions.");

}

// this function is to test processing commands received through simulated APRS
int get_buffer(char *buffer, int size)
{
  static int iteration = 0;
  switch (iteration)
  {
    case 0 ... 5: // no data for the first 6 calls
      return 0;
      break;
	case 6:
		{
			strncpy (buffer + 1, "S 3 50 {14", 9);
			unsigned char length = 9;
			buffer[0] = length;
			return 7;  
    }
  }


}