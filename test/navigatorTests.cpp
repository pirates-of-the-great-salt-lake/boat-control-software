#include <cstddef>
#include "engineer.h"
#include <iostream>
#include <unistd.h>
#include "navigator.h"
#include "engineer.h"

using namespace std;

void test_createpoint()
{
	nav_init();
	Nav_Point a = { {40.7106655, -111.9470059}, NULL, NULL, Active };
	Nav_Point b = { {40.7110165, -111.9466314}, NULL, NULL, Active };
	Nav_Point c = { {40.7107425, -111.9461252}, NULL, NULL, Active };
	//Nav_Point c = { {40.7163698, -111.9378532}, NULL, NULL, Active };

	cout << "Adding 3 points to the nav list:\n";
	nav_path_add_path(&a);
	nav_path_insert_path(1, &b);
	nav_path_insert_path(2, &c);
	//nav_path_insert_path(2, &c);
	//nav_print_path();

	//cout << "Adding a point to the head of list:\n";
	//Nav_Point pre_a = { {0, 0}, NULL, NULL, Active };
	//nav_path_insert_path(0, &pre_a);
	//nav_print_path();

	//cout << "Clearing all the abandoned points from nav list:\n";
	//nav_clear_abandoned_navpoints();
	//nav_print_path();

	//cout << "Clearing all the points from nav list:\n";
	//nav_clear_all_navpoints();
	//nav_print_path();

	nav_set_mode(Waypoint_Path);
	nav_startup(0);
	eng_output_set_val(Motor_Both, 70);
	eng_set_mode(Autonomous);
	return;
}

void test_lake_figure8 ()
{
	nav_init();
	Nav_Point a, b, c, d, e, f, g, h, i;
	cout << "Adding points for figure 8 test\n";
	a = { {40.710537, -111.946773}, NULL, NULL, Active, 10 };
	b = { {40.710728, -111.947469}, NULL, NULL, Active, 10 };
	c = { {40.711058, -111.946960}, NULL, NULL, Active, 10 };
	d = { {40.711518, -111.946634}, NULL, NULL, Active, 10 };
	e = { {40.711706, -111.947317}, NULL, NULL, Active, 10 };
	f = { {40.711350, -111.947727}, NULL, NULL, Active, 10 };
	g = { {40.711058, -111.946960}, NULL, NULL, Active, 10 };
	h = { {40.710779, -111.946530}, NULL, NULL, Active, 10 };
	i = { {40.710491, -111.946174}, NULL, NULL, Active, 10 };

	nav_path_add_path(&a);
	nav_path_insert_path(1,&b);
	nav_path_insert_path(2,&c);
	nav_path_insert_path(3,&d);
	nav_path_insert_path(4,&e);
	nav_path_insert_path(5,&f);
	nav_path_insert_path(6,&g);
	nav_path_insert_path(7,&h);
	nav_path_insert_path(8,&i);
	nav_print_path();
	nav_set_mode(Waypoint_Path);
	nav_set_optionf(Max_Speed, 120);
	nav_startup(0);
}

void test_backyard ()
{
	nav_init();
	Nav_Point a = { {40.7165654, -111.9381231}, NULL, NULL, Active, -10 };
	Nav_Point b = { {40.7163573, -111.9378931}, NULL, NULL, Active, 10 };
	nav_path_add_path(&a);
	nav_path_insert_path(1, &b);
	nav_set_mode(Waypoint_Path);
	nav_startup(0);
	eng_set_mode(Autonomous);
	return;
}



void test_linklist()
{
	nav_init();
	Nav_Point s, a, b, c, d;
	a = { {1, 1}, &b, NULL, Completed, -10}; 
	b = { {2, 2}, &c, &a, Completed, -10};
	c = { {3, 3}, NULL, &b, Completed, -10};

	nav_path_add_path(&a);
	nav_print_path();
	
	s = { {0, 0}, NULL, NULL, Active, 10};
	d = { {4, 4}, NULL, NULL, Active, 100};
	nav_path_insert_path(0, &s);
	nav_path_insert_path(4, &d);
	nav_print_path();
	
	nav_clear_all_navpoints();
	nav_print_path();

	nav_loadstate();
	nav_print_path();
	
	nav_shutdown();
}

/*
 * Testing the values for the distance and the bearing functions
 */
void test_dist_bearing ()
{
       point p1 = { 40.710553, -111.946909 };
       point p2 = { 40.710914, -111.947914 };
       
       cout << "Distance between two points: " << distance(p1, p2) << "\n";
       cout << "Distance between two points: " << bearing(p1, p2) << "\n";
}

/*
 * Spin the boat around in water for mag calibration
 */
void nav_mag_cal()
{
	cout << "Start engineer setup...\n";
	eng_init();
	eng_startup(Manual);
	eng_do_mag_cal();
	eng_output_set_val(Motor_Both, 70);
	eng_output_set_val(Servo_R, 45);
	cout << "Mag cal...\n";
	sleep(20);
	eng_end_mag_cal();
	cout << "Mag cal finished.\n";
}

int main()
{
//	nav_mag_cal();
	cout << "Staring the navigator testing...\n";

	//test_createpoint();
	//test_dist_bearing();
	//test_backyard();
/*	
	int loops = 1;
	for(int i = 0; i < loops; i++)
	{
		test_lake_figure8();
		while(1){
		}	
	}
*/
	test_linklist();	
	return 0;
}
