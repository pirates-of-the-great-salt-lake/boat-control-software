
IDIR =./include
CC = g++ 
CFLAGS += -I$(IDIR)
SRC = ./src
TST = ./test
TSTOBJ = ./build

OBJ = ./build
LDIR =./lib

BINDIR = ./bin
DEBUG = 

LIBS = -lm -liobb -lmodbus -lgps -lstdc++ -lpthread -lgpiod -lcurses

TESTS = $(wildcard $(TST)/*.c)
TESTOBJECTS := $(patsubst $(TST)/%.c, $(TSTOBJ)/%.o, $(TESTS))

C_SOURCES := $(wildcard $(SRC)/*.c)
CXX_SOURCES := $(wildcard $(SRC)/*.cpp)
C_OBJECTS := $(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(C_SOURCES))
CXX_OBJECTS := $(patsubst $(SRC)/%.cpp, $(OBJ)/%.o, $(CXX_SOURCES))
DEBUG = ""

all: $(C_OBJECTS) $(CXX_OBJECTS)
	$(CC) $(CFLAGS) $^ -o bcsd $(LIBS) -O0 -ggdb

.PHONY: clean no_motor del_capt

del_capt:
	rm -f build/captain.o

no_motor: CFLAGS += -DMAX_MOTOR_SPEED=0 

no_motor: del_capt $(C_OBJECTS) $(CXX_OBJECTS)
	$(CC) $(CFLAGS) $(C_OBJECTS) $(CXX_OBJECTS) -o bcsd $(LIBS) -O0 -ggdb

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(CFLAGS) -I$(IDIR) -c $< -o $@  -O0 -ggdb

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) $(CFLAGS) -I$(IDIR) -c $< -o $@  -O0 -ggdb

$(TSTOBJ)/%.o: $(TST)/%.c
	$(CC) $(CFLAGS) -I$(IDIR) -c $< -o $@ -O0 -ggdb

$(TSTOBJ)/%.o: $(TST)/%.cpp
	$(CC) $(CFLAGS) -I$(IDIR) -c $< -o $@ -O0 -ggdb

test_obj: $(TESTOBJECTS) $(C_OBJECTS)
	$(CC) $(OBJECTS) $^ -o $@

tests: $(OBJECTS) test_obj

eng_test: CFLAGS += -DDEBUG

eng_test: $(CXX_OBJECTS) $(C_OBJECTS) ./build/engineerTests.o
	$(CC) -I$(IDIR) build/pid.o build/MLX90393_compass.o build/engineerTests.o build/accelerometer.o build/engineer.o -o eng_test $(LIBS) -ggdb

nav_test: $(CXX_OBJECTS) $(C_OBJECTS) ./build/navigatorTests.o
	$(CC) -I$(IDIR) build/pid.o build/MLX90393_compass.o build/accelerometer.o build/navigatorTests.o build/engineer.o build/navigator.o -o nav_test $(LIBS) -ggdb

aprs: ./build/aprs.o ./build/aprs_test.o
	$(CC) build/aprs.o build/aprs_test.o -o aprs_test

hellomake: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)


clean:
	rm -f $(OBJ)/*.o *~ core $(INCDIR)/*~
